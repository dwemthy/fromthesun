﻿using UnityEngine;
using System.Collections;
using PhotonEnum;

public class GoldenAppleOfTheSun : MonoBehaviour {
	private const int NUM_COLORS = 8;
	private const int COLOR_RANGE = 3;

	public float speed;
	public float baseScale = 0.07f;
	public GameObject strangePrefab;
	public GameObject deathEffectPrefab;

	private PhotonColor photonColor;

	private Rigidbody rigiBod;

	public PhotonColor RandomizeColor(){
		int min = (int)PhotonColor.ULTRAVIOLET;
		int max = (int)DataCenter.Instance.PlayerColor + 1;
		int newColor = Random.Range(min, max);
		SetPhotonColor(newColor);

		return (PhotonColor)newColor;
	}

	void Start() {
		rigiBod = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update (){
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			RecalculateSpeed ();
			rigiBod.velocity = new Vector3 (0, 0, -speed);
		} else {
			rigiBod.velocity = new Vector3 ();
		}
	}

	void OnTriggerEnter(Collider other){
		if (transform.gameObject.activeSelf) {
			string otherTag = other.tag;
			
			if (otherTag.Equals ("Photon")) {
				GoldenAppleOfTheSun otherPhoton = other.GetComponent<GoldenAppleOfTheSun> ();
				if (otherPhoton != null) {
					if (otherPhoton.GetIntPhotonColor () >= GetIntPhotonColor ()) {
						SetPhotonColor ((int)Mathf.Min (GetIntPhotonColor () + 1, (int)PhotonColor.INFRARED));
					}else if(otherPhoton.GetIntPhotonColor() < GetIntPhotonColor ()) {
						SetPhotonColor((int)Mathf.Max (GetIntPhotonColor() - 1, (int)PhotonColor.ULTRAVIOLET));
					}
					other.transform.root.gameObject.SetActive (false);
				}
			}else if(otherTag.Equals("Planet")){
				transform.root.gameObject.SetActive(false);
			}
		}
	}

	private void RecalculateSpeed(){
		PhotonColor playerColor = DataCenter.Instance.PlayerColor;
		speed = GetIntPhotonColor () - (int)playerColor;
		if(speed == 0){
			speed = -1f;
		}
	}

	public Color GetColor(){
		return GetPhotonColor ().GetColor ();
	}

	public PhotonColor GetPhotonColor(){
		return photonColor;
	}

	public int GetIntPhotonColor(){
		return (int)photonColor;
	}

	public void SetPhotonColor(int color){
		SetPhotonColor ((PhotonColor)color);
	}

	public void SetPhotonColor(PhotonColor color){
		RecalculateSpeed ();
		photonColor = color;

		//float colorScale = PhotonUtils.scaleForColor (color);
		//float scale = baseScale * colorScale;
		//transform.localScale = new Vector3(scale, scale, scale);
	
		MeshRenderer renderer = GetComponent<MeshRenderer> ();
		if (renderer != null) {		
			renderer.material.color = GetColor();
		}
	}
}