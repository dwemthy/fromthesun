﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EndScreen : MonoBehaviour {
	public Image endImage;
	public Sprite winImage;
	public Sprite loseImage;
	public Text actionLabel;

	bool won = false;
	bool nextAvailable = false;
	
	public void Show(bool won, bool nextAvailable){
		this.won = won;
		this.nextAvailable = nextAvailable;
		gameObject.SetActive(true);
		if(won){
			endImage.sprite = winImage;
			if (nextAvailable) {
				actionLabel.text = "NEXT LEVEL";
			} else {
				actionLabel.text = "PLAY AGAIN";
			}
		}else{
			endImage.sprite = loseImage;
			actionLabel.text = "RETRY";
		}
	}

	public void Action(){
		if (won) {
			if (nextAvailable) {
				LevelManager.Instance.GoToNextLevel ();
			} else {
				LevelManager.Instance.Restart ();
			}
		} else {
			//Retry
			LevelManager.Instance.Restart();
		}
	}
}