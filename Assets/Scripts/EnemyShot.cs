﻿using UnityEngine;
using System.Collections;
using PhotonEnum;

public class EnemyShot : MonoBehaviour {
	public bool destroyDebris = true;

	private int power;
	public int Power {
		set { power = value; }
		get { return power; }
	}

	public int Damage(int damage){
		int dealt = damage;
		if (damage >= power) {
			dealt = power;
			transform.root.gameObject.SetActive(false);
		} else {
			power -= dealt;
		}

		return dealt;
	}

	void Update () {
		PhotonColor colorToSet = (PhotonColor)Random.Range(1, 9);
		Color color = colorToSet.GetColor();

		MeshRenderer renderer = GetComponent<MeshRenderer> ();
		if (renderer != null) {		
			renderer.material.color = color;
		}
	}

	void OnTriggerEnter(Collider other){
		if(DataCenter.Instance.GameState == DataCenter.STATE_STARTED){
			if (other.gameObject.tag.Equals ("Player")) {
				PlayerPhotonController player = other.gameObject.GetComponent<PlayerPhotonController> ();
				if (player != null) {
					player.Damage (power);
					gameObject.SetActive (false);
				}
			} else if (other.gameObject.tag.Equals ("Photon")) {
				other.gameObject.SetActive (false);
			} else if (other.gameObject.tag.Equals ("Shot")) {
				ShotController shot = other.gameObject.GetComponent<ShotController> ();
				if (shot != null) {
					int dealt = shot.Damage (power);
					power -= dealt;
					if (power <= 0) {
						gameObject.SetActive (false);
					}
				}
				other.gameObject.SetActive (false);
			} else if (other.gameObject.tag.Equals ("Debris") && destroyDebris) {
				Destructable dest = other.gameObject.GetComponent<Destructable> ();
				if (dest != null) {
					int dealt = dest.Damage (power);
					power -= dealt;
					if (power <= 0) {
						gameObject.SetActive (false);
					}
				} else {
					Core core = other.gameObject.GetComponent<Core> ();
					if (core != null) {
						int dealt = core.Damage (power);
						power -= dealt;
						if (power <= 0) {
							gameObject.SetActive (false);
						}
					}
				}
			}
		}
	}
}