﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemnentDestructable : Destructable {
    public GameObject remnentPrefab;
    public int remnentCount = 1;
	public float remnentSpeed = 2;
	public float remnentSpread = Mathf.PI;

    protected override void Die() {
        base.Die();

        SpawnRemnents();
    }

    protected virtual void SpawnRemnents() {
		for(int i=0; i < remnentCount; i++) {
			GameObject remnent = Instantiate(remnentPrefab);
			float spawnAngle = Random.Range(0, remnentSpread);
			Vector3 spawnVelocity = new Vector3(Mathf.Cos(spawnAngle), 0, -Mathf.Sin(spawnAngle));
			spawnVelocity *= remnentSpeed;

			ObjectMover mover = remnent.GetComponent<ObjectMover>();
			if (mover != null) {
				mover.velocity = spawnVelocity;
			}

			remnent.transform.position = transform.position + spawnVelocity.normalized;
		}
    }
}
