﻿using UnityEngine;
using System.Collections;

public class Absorber : MonoBehaviour {
	public PoweredModule poweredModule;
	private Destructable host;

	void Start(){
		host = GetComponentInParent<Destructable> ();
	}

	void OnTriggerEnter(Collider other){
		bool alive = true;
		if (host != null) {
			alive = host.Health > 0;
		}

		if(DataCenter.Instance.GameState == DataCenter.STATE_STARTED && alive){
			if (other.gameObject.tag.Equals ("Photon")) {
				other.gameObject.SetActive (false);
			}else if(other.gameObject.tag.Equals("Shot")){
				other.gameObject.SetActive (false);
				if (poweredModule != null) {
					ShotController shot = other.gameObject.GetComponent<ShotController> ();
					if (shot != null) {
						poweredModule.AddPower (shot.power());
					}
				}
			} else if (other.gameObject.tag.Equals ("Player")) {
				PlayerPhotonController player = other.gameObject.GetComponent<PlayerPhotonController> ();
				if (player != null) {
					player.Damage (host != null ? host.Health : 1);
				} else {
					other.gameObject.SetActive (false);
				}
			}
		}
	}
}