﻿using UnityEngine;
using System.Collections;
using PhotonEnum;

public class PlayerColorChanger : MonoBehaviour {
	private Animator animator;
	private int color;

	void Start () {
		animator = GetComponent<Animator> ();
		color = (int)DataCenter.Instance.PlayerColor;
	}

	void FixedUpdate(){
		int currentColor = (int)DataCenter.Instance.PlayerColor;
		if (currentColor != color && animator != null) {
			color = currentColor;
			animator.SetInteger ("Color", color);
		}
	}
}
