﻿using UnityEngine;
using System.Collections;

public class TutorialController : MonoBehaviour {
    public GameObject movementUI;
    public GameObject colorChangeUI;
    public GameObject ammoUI;
    public GameObject debrisUI;
    public GameObject crystalUI;

    public float instructionDuration;

    public float timeToShowColor;
    public float timeToShowAmmo;
    public float timeToShowDebris;
    public float timeToShowCrystal;
    public float endTime;

    private float timePlayed = 0f;
    private float timeShowingInstruction = 0f;

    void Start() {
        movementUI.SetActive(true);
        timePlayed = 0f;
		timeShowingInstruction = 0f;
		DataCenter.Instance.GameState = DataCenter.STATE_STARTED;
    }

	void Update () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			timePlayed += LevelManager.ScaledTime();
			timeShowingInstruction += LevelManager.ScaledTime();

			if(timePlayed >= endTime) {
				LevelManager.Instance.Win ();
				DataCenter.Instance.ClearSession ();
			}else if(timePlayed >= timeToShowCrystal){
				if(timePlayed > timeToShowCrystal + instructionDuration) {
					crystalUI.SetActive(false);
				}else if(!crystalUI.activeSelf){
					crystalUI.SetActive(true);
				}
			}else if(timePlayed >= timeToShowDebris) {
				if(timePlayed > timeToShowDebris + instructionDuration) {
					debrisUI.SetActive(false);
				}else if(!debrisUI.activeSelf){
					debrisUI.SetActive(true);
				}
			}else if(timePlayed >= timeToShowAmmo) {
				if (timePlayed > timeToShowAmmo + instructionDuration) {
					ammoUI.SetActive(false);
				} else if (!ammoUI.activeSelf) {
					ammoUI.SetActive(true);
				}
			}else if(timePlayed >= timeToShowColor) {
				if(timePlayed > timeToShowColor + instructionDuration) {
					colorChangeUI.SetActive(false);
				}else if (!colorChangeUI.activeSelf) {
					colorChangeUI.SetActive(true);
				}
			}else if(timePlayed > instructionDuration) {
				movementUI.SetActive(false);
			}
		}
	}
}