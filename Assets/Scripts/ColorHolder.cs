﻿using UnityEngine;
using System.Collections;
using PhotonEnum;

public class ColorHolder : MonoBehaviour {
	public PhotonColor photonColor;

	public void SetColor(int color){
		SetPhotonColor ((PhotonColor)color);
	}

	public void SetPhotonColor(PhotonColor color){
		photonColor = color;

		MeshRenderer renderer = GetComponent<MeshRenderer> ();
		if (renderer != null) {
			renderer.material.color = photonColor.GetColor();
		}else{
			renderer = GetComponentInChildren<MeshRenderer>();
			if (renderer != null){
				renderer.material.color = photonColor.GetColor();
			}
		}
	}

	public bool isKilledByPlayer(){
		bool killed = false;
		PhotonColor playerColor = DataCenter.Instance.PlayerColor;
		if(playerColor == PhotonColor.ULTRAVIOLET){
			//If the photon is ultraviolet then it will pass through all but 
			//infrared transparant debris
			if(photonColor == PhotonColor.INFRARED){
				killed = true;
			}
		}else if(playerColor == PhotonColor.INFRARED){
			//If the photon is infrared then it will only pass through infrared
			if(photonColor != PhotonColor.INFRARED){
				killed = true;
			}
		}else{
			//If the photon is a non-extreme color
			if(photonColor == PhotonColor.ULTRAVIOLET){
				//Allow all colors of photon through
			}else if(photonColor == PhotonColor.INFRARED){
				//Block all standard colors
				killed = true;
			}else if(photonColor != DataCenter.Instance.PlayerColor) {
				//Destroy all non-matching colors
				killed = true;
			}
		}

		return killed;
	}

	public bool hitsColor(PhotonColor color){
		bool hit = false;
		if(color == PhotonColor.ULTRAVIOLET){
			//Allow all colors of photon through
		}else if(color == PhotonColor.INFRARED){
			//Block all standard colors
			hit = true;
		}else if(color != photonColor){
			//Destroy all non-matching colors
			hit = true;
		}

		return hit;
	}
}
