﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using PhotonEnum;

public class PlayerPhotonController : MonoBehaviour {
	private const int NUM_COLORS = 8;
	private const int PHOTON_SCORE = 1;

	public float speed;

	public Transform ammoAnimationDestination;

	public AudioSource damagedSource;

	public float shotCooldown = 0.1f;
	private float timeSinceShot;

	public float baseScale = 0.09f;
	public float baseEmission = 30f;

	private Rigidbody rigiBod;
	private Light playerGlow;
	private TrailRenderer trail;

	void Start () {
		//GET COMPONENTS
		rigiBod = GetComponent<Rigidbody> ();
		playerGlow = GetComponent<Light> ();

		MeshRenderer renderer = GetComponent<MeshRenderer> ();
		if (renderer != null) {	
			renderer.sharedMaterial.color = GetColor();
		}

		trail = GetComponent<TrailRenderer> ();

		//INITIALIZE VALUES
		SetPhotonColor(DataCenter.Instance.PlayerColor);

		timeSinceShot = shotCooldown;

		//INITIALIZE GAME STATE
        DataCenter.Instance.GameState = DataCenter.STATE_STARTED;
		DataCenter.Instance.PlayerPosition = transform.position;
		Time.timeScale = 1f;
	}

	void FixedUpdate () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			timeSinceShot += Time.deltaTime;

			updateVelocity ();
			UpdateEnergy ();

			DataCenter.Instance.PlayerPosition = transform.position;
		}
	}
	
	private void updateVelocity(){
		float xDelta;
		float zDelta;
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
			Vector2 touchDeltaNormalized = Input.GetTouch(0).deltaPosition.normalized;
			xDelta = touchDeltaNormalized.x * speed;
			zDelta = touchDeltaNormalized.y * speed;
		}else{
			xDelta = Input.GetAxis ("Horizontal") * speed;
			zDelta = Input.GetAxis ("Vertical") * speed;
		}
		
		rigiBod.velocity = new Vector3(xDelta, 0, zDelta);
	}
	
	private void UpdateEnergy(){
		int energy = DataCenter.Instance.Ammo;
		
		if(timeSinceShot >= shotCooldown && Input.GetKey(KeyCode.Space) && energy > 0){
			timeSinceShot = 0;
			energy--;

			GameObject shot = ObjectPoolingManager.Instance.GetShot();
			shot.transform.position = transform.position;
			
			ShotController shotCont = shot.GetComponent<ShotController>();
			if(shotCont != null){
				shotCont.Activate ();
				shotCont.SetPhotonColor(DataCenter.Instance.PlayerColor);
			}
		}
		
		if (energy <= 0) {
			energy = 0;
		}

		DataCenter.Instance.Ammo = energy;
	}
	
	public void SetPhotonColor(int color){
		SetPhotonColor ((PhotonColor)color);
	}
	
	public void SetPhotonColor(PhotonColor color) {
        DataCenter.Instance.PlayerColor = color;
		
	//	MeshRenderer renderer = GetComponent<MeshRenderer> ();
	//	if (renderer != null) {	
	//		renderer.sharedMaterial.color = GetColor();
    //    }

		if (playerGlow != null) {
			playerGlow.color = color.GetColor ();
		}

	//	float colorScale = PhotonUtils.scaleForColor (color);


	//	if (trail != null) {
	//		trail.startWidth = colorScale;
	//		trail.endWidth = colorScale * 0.1f;
	//	}
	}
	
	private Color GetColor(){
		return DataCenter.Instance.PlayerColor.GetColor ();
	}
	
	void OnTriggerEnter(Collider other){
		if(DataCenter.Instance.GameState == DataCenter.STATE_STARTED){
			if(other.tag.Equals("Photon")){
				GoldenAppleOfTheSun preyPhoton = other.GetComponent<GoldenAppleOfTheSun> ();
				if (preyPhoton != null){
					other.transform.root.gameObject.SetActive(false);
					
					GameObject deathEffect = ObjectPoolingManager.Instance.GetPhotonDestruction();
					deathEffect.transform.position = other.transform.position;
					
					ParticleSystem particles = deathEffect.GetComponent<ParticleSystem>();
					particles.startColor = preyPhoton.GetColor();

					MoveToPosition move = deathEffect.GetComponent<MoveToPosition> ();
					move.SetDestination (ammoAnimationDestination.position);

					AudioSource pickup = deathEffect.GetComponent<AudioSource> ();
					pickup.Play ();

					if(DataCenter.Instance.PlayerColor > PhotonColor.ULTRAVIOLET &&
						DataCenter.Instance.PlayerColor > preyPhoton.GetPhotonColor()){
						PhotonColor newColor = (PhotonColor)((int)DataCenter.Instance.PlayerColor - 1);
						SetPhotonColor(newColor);
					}

					DataCenter.Instance.Ammo = DataCenter.Instance.Ammo + 1;
                }
			}
		}
	}

	public void Damage(int damage){
		if(DataCenter.Instance.PlayerColor == PhotonColor.INFRARED){
			LevelManager.Instance.Lose ();
		}else{
			int newColor = (int)DataCenter.Instance.PlayerColor + damage;
			if(newColor > 8){
				newColor = 8;
			}

			SetPhotonColor((PhotonEnum.PhotonColor)newColor);
			CameraShake shake = Camera.main.GetComponent<CameraShake> ();
			shake.shakeDuration = 0.1f;

			damagedSource.Play ();

		}
	}
}