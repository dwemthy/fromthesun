using System;
using System.Collections.Generic;
using UnityEngine;
using WaveEnum;

[Serializable]
public class Wave{
	public WaveType waveType;
	public List<Spawn> spawns = new List<Spawn>();
	public float timeTrigger;
    public PhotonEnum.PhotonColor color;
	public float duration = 0;
	public float frequency = 0;
	public GameObject prefab;
	public SplineSpawnData splineData = new SplineSpawnData();
	
	public static List<Wave> GenerateNByNRockWaves(int n, float minX, float maxX){
		List<int> order = new List<int>();
		for(int i = 0 ; i < n; i++){
			order.Add(i);
		}
		
		Utils.Shuffle(order);
		
		List<Wave> waves = new List<Wave>();
		
		float range = maxX - minX;
		float startIncrement = range / (float)n;
		float step = range / (float)(n*n);
		
		for(int i = 0; i < n; i++){
			int startIndex = order[i];
			float start = minX + (startIndex * startIncrement);
			
			Wave wave = new Wave();
			wave.spawns = new List<Spawn>();
			for(int j = 0; j < n; j++){
				Spawn spawn = new Spawn();
				Vector3 pos = new Vector3();
				pos.z = j;
				pos.x = start + (order[j] * step);
				
				spawn.position = pos;
				spawn.type = SpawnEnum.SpawnType.ROCK;
				spawn.scale = UnityEngine.Random.Range(0.7f, 1f);
				spawn.velocity = new Vector3 (0, 0, -4);
				wave.spawns.Add(spawn);
			}
			waves.Add(wave);
		}
		
		return waves;
	}

    public static Wave GenerateWaveWithCrystals(float minX, float maxX, PhotonEnum.PhotonColor color){
        List<int> order = new List<int>();
        for (int i = 0; i < 5; i++){
            order.Add(i);
        }

        Utils.Shuffle(order);

        int center = UnityEngine.Random.Range(0, 3);

        Wave crystalWave = new Wave();
        crystalWave.spawns = new List<Spawn>();

        float range = maxX - minX;
        float startStep = range * 0.333f;
        float step = range / 15f;
        float start = minX + startStep * center;

        for(int i=0; i<5; i++){
            Spawn spawn = new Spawn();
            Vector3 pos = new Vector3();
            pos.z = i;
            pos.x = start + (order[i] * step);
            spawn.color = color;

            spawn.position = pos;
            spawn.type = SpawnEnum.SpawnType.CRYSTAL;
            spawn.scale = 1f;
            crystalWave.spawns.Add(spawn);
        }

        return crystalWave;
    }

	public static Wave GenerateRockHelix(float minX, float maxX, float frequency, int iterations){
		Wave helixWave = new Wave ();
		helixWave.spawns = new List<Spawn> ();

		float range = (maxX - minX) * 0.5f;
		float centerX = minX + range;

		int rows = (int)(frequency * iterations);
		float stepSize = (float)Math.PI / frequency;
		float theta = 0;
		for (int i = 0; i < rows; i++) {
			float sin = (float)Math.Sin (theta);
			Spawn left = new Spawn ();
			Vector3 pos = new Vector3 ();
			pos.z = i;
			pos.x = centerX - (sin * range);

			left.position = pos;
			left.type = SpawnEnum.SpawnType.ROCK;
			left.scale = UnityEngine.Random.Range (0.9f, 1.1f);
			left.velocity = new Vector3 (0, 0, -4);
			helixWave.spawns.Add (left);

			Spawn right = new Spawn ();
			pos.x = centerX + (sin * range);
			pos.z += 0.5f;
			right.position = pos;
			right.type = SpawnEnum.SpawnType.ROCK;
			right.scale = UnityEngine.Random.Range (0.9f, 1.1f);
			right.velocity = new Vector3 (0, 0, -4);
			helixWave.spawns.Add (right);

			theta += stepSize;
		}

		return helixWave;
	}

	public static Wave GenerateFiveOfPrefab(float minX, float maxX, float spacing, GameObject prefab){
		List<int> order = new List<int>();
		for(int i = 0 ; i < 5; i++){
			order.Add(i);
		}

		Utils.Shuffle(order);

		Wave fiveOf = new Wave ();
		fiveOf.spawns = new List<Spawn> ();

		float range = maxX - minX;
		float startIncrement = range * 0.2f;

		for(int i = 0; i < 5; i++){
			int startIndex = order[i];
			float start = minX + (startIndex * startIncrement);

			Spawn spawn = new Spawn();
			Vector3 pos = new Vector3(start, 0, i * spacing);
			spawn.position = pos;
			spawn.type = SpawnEnum.SpawnType.PREFAB;
			spawn.prefab = prefab;
			fiveOf.spawns.Add(spawn);
		}

		return fiveOf;
	}
}