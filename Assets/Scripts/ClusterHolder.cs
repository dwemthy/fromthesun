﻿using UnityEngine;
using System.Collections.Generic;

public class ClusterHolder : MonoBehaviour {
	protected Transform[] children;

	// Use this for initialization
	public virtual void Start () {
		children = GetComponentsInChildren<Transform> ();
	}

	public virtual void FixedUpdate () {
		int activeChildren = 0;
		for (int i = 0; i < children.Length; i++) {
			if (children [i].gameObject.activeInHierarchy) {
				activeChildren++;
			}
		}

		if (activeChildren == 1) {//1 == just this
			PlanetBehavior planet = GetComponent<PlanetBehavior> ();
			if (planet != null) {
				planet.Die ();
			} else {
				transform.gameObject.SetActive (false);
			}
		}
	}

	public void SetMaxHealth(int maxHealth){
		Destructable[] destructables = GetComponentsInChildren<Destructable> ();
		for (int i = 0; i < destructables.Length; i++) {
			destructables [i].SetMaxHealth (maxHealth);
		}
	}
}
