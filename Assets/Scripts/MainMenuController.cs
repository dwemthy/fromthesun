﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuController : MonoBehaviour {
    private const bool UNLOCKED = true;

	public const string TUTORIAL_LEVEL = "Tutorial";
	public const string MERCURY_LEVEL = "Mercury";
	public const string VENUS_LEVEL = "Venus";
	public const string EARTH_LEVEL = "Earth";
	public const string TEST_LEVEL = "MercuryTest";

	private string selectedLevel = "Mercury";

	public Toggle mercuryToggle;
	public GameObject mercuryFlare;
	public Toggle venusToggle;
	public GameObject venusFlare;
	public Toggle earthToggle;
	public GameObject earthFlare;

	public Text selectedLevelLabel;

	public Toggle testToggle;

    void Start() {
		DataCenter.Instance.ClearSession ();
		DataCenter.Instance.GameState = DataCenter.STATE_UNSTARTED;

		//Pre-disable all selection effects
		mercuryToggle.isOn = false;
		mercuryFlare.SetActive(false);

		venusToggle.isOn = false;
		venusFlare.SetActive(false);

		earthToggle.isOn = false;
		earthFlare.SetActive(false);


		string lastUnlocked = MERCURY_LEVEL;
		if (IsLevelUnlocked (VENUS_LEVEL)) {
			lastUnlocked = VENUS_LEVEL;
			venusToggle.gameObject.SetActive (true);

			if (IsLevelUnlocked (EARTH_LEVEL)) {
				lastUnlocked = EARTH_LEVEL;

				earthFlare.SetActive(true);
				earthToggle.gameObject.SetActive(true);
				earthToggle.isOn = true;
			} else {
				venusFlare.SetActive (true);
				venusToggle.isOn = true;

				earthToggle.gameObject.SetActive (false);
			}
		} else {
			mercuryToggle.isOn = true;

			venusToggle.gameObject.SetActive (false);

			earthToggle.gameObject.SetActive (false);
		}

		SetSelectedLevel (lastUnlocked);
    }
    
	public void ShowTutorial() {
		//DataCenter.Instance.GameState = DataCenter.STATE_STARTED;
		Application.LoadLevel(TUTORIAL_LEVEL);
    }

    public void Play() {
        //DataCenter.Instance.GameState = DataCenter.STATE_STARTED;
        Application.LoadLevel(selectedLevel);
    }

	public void OnMercurySelectedChanged(bool selected){
		if (mercuryToggle.isOn) {
			SetSelectedLevel (MERCURY_LEVEL);
			mercuryFlare.SetActive (true);
		} else {
			mercuryFlare.SetActive (false);
		}
	}

	public void OnVenusSelectedChanged(bool selected){
		if (venusToggle.isOn) {
			SetSelectedLevel (VENUS_LEVEL);
			venusFlare.SetActive (true);
		} else {
			venusFlare.SetActive (false);
		}
	}

	public void OnEarthSelectedChanged(bool selected){
		if (earthToggle.isOn) {
			SetSelectedLevel (EARTH_LEVEL);
			earthFlare.SetActive (true);
		} else {
			earthFlare.SetActive (false);
		}
	}

	public void OnTestSelectedChanged(bool selected){
		if (testToggle.isOn) {
			SetSelectedLevel (TEST_LEVEL);
		}
	}

	void SetSelectedLevel(string levelName){
		selectedLevel = levelName;
		selectedLevelLabel.text = "Attack " + levelName;
	}

	bool IsLevelUnlocked(string levelName){
		return UNLOCKED || PlayerPrefs.GetInt (levelName) > 0;
	}
}