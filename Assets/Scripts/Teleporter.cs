﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {
	public GameObject destination;
	public float offset;
	
	void OnTriggerEnter(Collider other){
		Vector3 pos = other.transform.position;
		pos.x = destination.transform.position.x + offset;
		other.transform.position = pos;
	}
}
