﻿using UnityEngine;
using System.Collections;

public class PlanetPiece : MonoBehaviour {
	void Start () {
		MeshRenderer renderer = GetComponent<MeshRenderer> ();
		if (renderer != null) {
			renderer.material.SetVector ("_PiecePos", transform.position);
		}
	}
}
