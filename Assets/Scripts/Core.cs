﻿using UnityEngine;
using System.Collections;

public class Core : Destructable {
	private Destructable host;

	// Use this for initialization
	void Start () {
		health = maxHealth;
		if (transform.parent != null) {
			host = transform.parent.gameObject.GetComponent<Destructable> ();
		}
	}

	public override int Damage(int damage){
		int dealt = damage;
		if (health <= 0) {
			dealt = 0;
		} else if (damage >= health) {
			if (destroyedSource != null) {
				destroyedSource.Play ();
			}

			LevelManager.Instance.AddScore (maxHealth * 50, transform.position);
			SpawnTetras ();

			if (host != null) {
				host.Damage (host.Health);
			}

			dealt = Health;
		}

		health -= dealt;

		return dealt;
	}
}