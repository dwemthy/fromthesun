﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using SpawnEnum;
using WaveEnum;
using PhotonEnum;

[CustomEditor(typeof(DebrisSpawner))]
public class DebrisSpawnerEditor : Editor {
	private DebrisSpawner spawner;
	private float halfWidth;
	private float halfHeight;
	private List<Wave> waves;
	private List<Wave> planetWaves;

	private int selectedWave = -1;
	private int selectedSpawn = -1;
	private int selectedPlanetWave = -1;

	private float timeScale = 4f;

	private void OnSceneGUI () {
		spawner = target as DebrisSpawner;
		halfWidth = spawner.transform.localScale.x * 0.5f;
		halfHeight = spawner.transform.localScale.z * 0.5f;
		waves = spawner.waves;
		planetWaves = spawner.planetWaves;
		float lastTimeTrigger = 0f;
		for(int i=0; i<waves.Count; i++){
			Wave wave = waves [i];
			int newSelection = drawWave (wave, i, selectedWave, 0, false);
			if (newSelection != selectedWave) {
				selectedWave = newSelection;
				selectedPlanetWave = -1;
			}
			lastTimeTrigger = wave.timeTrigger;
		}

		float endZ = spawner.transform.position.z + halfHeight + ((lastTimeTrigger + spawner.delayBetweenLastWaveAndPlanet) * timeScale);

		Handles.color = Color.yellow;
		Handles.DrawLine(new Vector3(spawner.transform.position.x + halfWidth, 0, 0),
			new Vector3(spawner.transform.position.x + halfWidth, 0, endZ));

		Handles.DrawLine(new Vector3(spawner.transform.position.x - halfWidth, 0, 0),
			new Vector3(spawner.transform.position.x - halfWidth, 0, endZ));

		Handles.color = Color.red;
		Vector3 planetLineEnd = new Vector3 (spawner.transform.position.x + halfWidth, 0, endZ);
		Handles.DrawLine(new Vector3(spawner.transform.position.x - halfWidth, 0, endZ),
				planetLineEnd);
		
		Quaternion handleRot = Quaternion.Euler(new Vector3());
		EditorGUI.BeginChangeCheck();
		Vector3 pos = Handles.DoPositionHandle(planetLineEnd, handleRot);
		if(EditorGUI.EndChangeCheck()){
			float zDiff = pos.z - planetLineEnd.z;
			zDiff /= timeScale;
			spawner.delayBetweenLastWaveAndPlanet += zDiff;
			if (spawner.delayBetweenLastWaveAndPlanet < 0) {
				spawner.delayBetweenLastWaveAndPlanet = 0;
			} else if (spawner.delayBetweenLastWaveAndPlanet > 100) {
				spawner.delayBetweenLastWaveAndPlanet = 100;
			}
		}

		GUIStyle style = new GUIStyle ();
		style.normal.textColor = Color.red;
		planetLineEnd.x += 2f;
        if(spawner.waves.Count > 0)
        {
            Handles.Label(planetLineEnd, "Planet at " + (spawner.delayBetweenLastWaveAndPlanet + spawner.waves[spawner.waves.Count - 1].timeTrigger) + " seconds", style);
        }

		Handles.DrawWireDisc (new Vector3 (spawner.transform.position.x, 0, spawner.transform.position.z + +halfHeight + ((lastTimeTrigger + spawner.delayBetweenLastWaveAndPlanet) * timeScale) + 5),
			Vector3.up,
			4f);
		for(int i=0; i<planetWaves.Count; i++){
			Wave wave = planetWaves [i];
			int newSelection =  drawWave (wave, i, selectedPlanetWave, planetLineEnd.z, true);
			if (newSelection != selectedPlanetWave) {
				selectedPlanetWave = newSelection;
				selectedWave = -1;
			}
		}

        SortWaves();
	}

	private int drawWave(Wave wave, int index, int selectedIndex, float zOffset, bool forPlanet){
		Handles.color = Color.white;
		Vector3 lineEnd = new Vector3 (spawner.transform.position.x + halfWidth, 0, spawner.transform.position.z + halfHeight + (wave.timeTrigger * timeScale) + zOffset);
		Handles.DrawLine(new Vector3(spawner.transform.position.x - halfWidth, 0, spawner.transform.position.z + halfHeight + (wave.timeTrigger * timeScale) + zOffset),
			lineEnd);

		lineEnd.x += 2f;
		GUIStyle style = new GUIStyle ();
		style.normal.textColor = Color.white;
		Handles.Label (lineEnd, "Wave #" + index + " @ " + wave.timeTrigger + " seconds", style);

		Quaternion handleRot = Quaternion.Euler(new Vector3());
		Vector3 pos = lineEnd;
		if(index == selectedIndex){
			EditorGUI.BeginChangeCheck();
			pos = Handles.DoPositionHandle(pos, handleRot);
			if(EditorGUI.EndChangeCheck()){
				float zDiff = (pos.z - lineEnd.z) / timeScale;
				if (forPlanet) {
					for (int i = index; i < planetWaves.Count; i++) {
						planetWaves [i].timeTrigger += zDiff;
					}
				} else {
					for (int i = index; i < waves.Count; i++) {
						waves [i].timeTrigger += zDiff;
					}
				}
				selectedIndex = index;
				selectedSpawn = -1;
			}
		}else{
			Vector3 point = spawner.transform.TransformPoint(lineEnd);
	        Handles.color = Color.green;
	        float size = HandleUtility.GetHandleSize(point);
			if(Handles.Button(lineEnd, handleRot, size * 0.04f, size * 0.06f, Handles.DotCap)){
				selectedIndex = index;
				selectedSpawn = -1;
				Repaint();
			}
		}

		float increment, start, z;
		float scaledTime = wave.timeTrigger * timeScale;

		switch (wave.waveType) {
		case WaveType.MANUAL:
			for(int i=0; i<wave.spawns.Count; i++){
				Spawn spawn = wave.spawns [i];
				selectedIndex = drawSpawn (spawn, wave.timeTrigger* timeScale, i, index, selectedIndex, zOffset, forPlanet);
			}
			break;

		case WaveType.ROCK_HELIX:
			float lowZ = spawner.transform.position.z + halfHeight + scaledTime;
			float step = (wave.frequency / Mathf.PI) * timeScale;
			float midX = spawner.transform.position.x;
			float lowX = midX - halfWidth;
			float highX = midX + halfWidth;
			for (int i = 0; i < wave.duration; i++) {
				Handles.color = Color.gray;
				Handles.DrawLine(new Vector3(midX, 0, lowZ),
					new Vector3(highX, 0, lowZ + step*0.5f));
				Handles.DrawLine(new Vector3(midX, 0, lowZ),
					new Vector3(lowX, 0, lowZ + step*0.5f));

				Handles.DrawLine(new Vector3(midX, 0, lowZ + step),
					new Vector3(lowX, 0, lowZ + step*0.5f));
				Handles.DrawLine(new Vector3(midX, 0, lowZ + step),
					new Vector3(highX, 0, lowZ + step*0.5f));

				lowZ += step;
			}
			break;

		case WaveType.FIVE_CRYSTAL:
			increment = halfWidth * 1f / 3f;
			start = spawner.transform.position.x - (increment * 2);
			z = spawner.transform.position.z + halfHeight + scaledTime;
			for (float x = start; x < start + (increment * 5); x += increment) {
				pos = new Vector3 (x, 0, z);
				drawCrystalAtPosition(pos, GetColorForSpawnType(SpawnType.CRYSTAL));
			}
			break;

		case WaveType.FIVE_OF_PREFAB:
			increment = halfWidth * 0.4f;
			start = spawner.transform.position.x - (increment * 2);
			z = spawner.transform.position.z + halfHeight + scaledTime + 2;
			for (float x = start; x < start + (increment * 5); x += increment) {
				pos = new Vector3 (x, 0, z);
				drawPrefabAtPosition (pos, wave.prefab, 1f);
				z += wave.frequency;
			}
			break;

		case WaveType.FIVE_TIMES_FIVE_ROCK:
			increment = halfWidth * 0.4f;
			start = spawner.transform.position.x - halfWidth;
			z = spawner.transform.position.z + halfHeight + scaledTime;
			step = increment * 0.2f;
			for (int i = 0; i < 5; i++) {
				for (int j = 4; j >= 0; j--) {
					Vector3 stepPos = new Vector3 (start + (i * increment) + (j * step), 0, z);
					drawRockAtPosition (stepPos, 0.5f);
					z += 0.6f;
				}
			}
			break;

		case WaveType.PREFAB_RAIN:
			increment = halfWidth * 0.4f;
			start = spawner.transform.position.x - (increment * 2);
			z = spawner.transform.position.z + halfHeight + scaledTime + 1;
			pos = new Vector3 (start, 0, z);
			for (int i = 0; i < wave.duration; i++) {
				drawPrefabAtPosition (pos, wave.prefab, 1f);
				pos.x = pos.x + increment;
				drawPrefabAtPosition (pos, wave.prefab, 1f);
				pos.x = pos.x + increment;
				drawPrefabAtPosition (pos, wave.prefab, 1f);
				pos.x = pos.x + increment;
				drawPrefabAtPosition (pos, wave.prefab, 1f);
				pos.x = pos.x + increment;
				drawPrefabAtPosition (pos, wave.prefab, 1f);
				pos.x = pos.x + increment;
				pos.x = start;
				pos.z += timeScale;
			}
			break;

		case WaveType.SPLINE:
			Vector3 splinePos = new Vector3 (spawner.transform.position.x, 0, spawner.transform.position.z + halfHeight + scaledTime);
			drawSplineAtPosition (splinePos, wave.splineData, index, zOffset, forPlanet);
			break;
		}

		return selectedIndex;
	}

	private int drawSpawn(Spawn spawn, float timeTrigger, int spawnIndex, int waveIndex, int selectedWaveIndex, float zOffset, bool forPlanet){
		Vector3 spawnPos = spawn.position;
		Vector3 UISpawnPos = spawnPos;
		UISpawnPos.x *= halfWidth;
		UISpawnPos.z += spawner.transform.position.z + halfHeight + timeTrigger + zOffset;

		switch (spawn.type) {
		case SpawnType.ROCK:
			drawRockAtPosition (UISpawnPos, spawn.scale);
			break;

		case SpawnType.CRYSTAL:
			drawCrystalAtPosition (UISpawnPos, spawn.color.GetColor());
			break;

		case SpawnType.DUST:
			drawDustAtPosition (UISpawnPos);
			break;

		case SpawnType.RADIATION:
			drawRadiationAtPosition (UISpawnPos);
			break;

		case SpawnType.PREFAB:
			drawPrefabAtPosition (UISpawnPos, spawn.prefab, spawn.scale);
			break;

		case SpawnType.SPLINE:
			drawSplineAtPosition (spawnPos, spawn.splineData, waveIndex, timeTrigger + zOffset, forPlanet);
			break;
		}

		Quaternion spawnRot = Quaternion.Euler(new Vector3());

		if(waveIndex == (forPlanet ? selectedPlanetWave : selectedWave) && spawnIndex == selectedSpawn){
			EditorGUI.BeginChangeCheck();
			Vector3 pos = Handles.DoPositionHandle(UISpawnPos, spawnRot);
			if(EditorGUI.EndChangeCheck()){
				selectedSpawn = spawnIndex;
				pos.x /= halfWidth;
				pos.z -= spawner.transform.position.z + halfHeight + timeTrigger + zOffset;
				spawn.position = new Vector3(pos.x, 0, pos.z);
			}
		}else{
			Vector3 point = spawner.transform.TransformPoint(UISpawnPos);
	        Handles.color = Color.green;
	        float size = HandleUtility.GetHandleSize(point);
			if(Handles.Button(UISpawnPos, spawnRot, size * 0.04f, size * 0.06f, Handles.DotCap)){
				if (forPlanet) {
					selectedWave = -1;
					selectedPlanetWave = waveIndex;
				} else {
					selectedPlanetWave = -1;
					selectedWave = waveIndex;
				}
				selectedWaveIndex = waveIndex;
				selectedSpawn = spawnIndex;
				Repaint();
			}
		}

		return selectedWaveIndex;
	}

	private void drawSplineAtPosition(Vector3 pos, SplineSpawnData splineData, int waveIndex, float zOffset, bool forPlanet){
		if (waveIndex == (forPlanet ? selectedPlanetWave : selectedWave)) {
			for (int i = 0; i < splineData.frequency; i++) {
				Vector3 spawnPos = splineData.GetPoint(spawner.transform, (float)i/splineData.frequency);
				spawnPos.x *= halfWidth;
				spawnPos.z += zOffset;
                spawnPos.x += pos.x * halfWidth;
				spawnPos.z += pos.z;
				drawRockAtPosition(spawnPos, splineData.scaleMin);
			}
		}

		Vector3 p0 = drawSplinePoint(pos, splineData, 0, waveIndex, zOffset, forPlanet);
		for(int i = 1; i < splineData.points.Length; i += 3) {
			Vector3 p1 = drawSplinePoint(pos, splineData, i, waveIndex, zOffset, forPlanet);
			Vector3 p2 = drawSplinePoint(pos, splineData, i + 1, waveIndex, zOffset, forPlanet);
			Vector3 p3 = drawSplinePoint(pos, splineData, i + 2, waveIndex, zOffset, forPlanet);

			if ((forPlanet ? selectedPlanetWave : selectedWave) == waveIndex) {
				Handles.color = Color.gray;
				Handles.DrawLine(p0, p1);
				Handles.DrawLine(p2, p3);
			}

			Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 2f);
			p0 = p3;
		}
	}

	private Vector3 drawSplinePoint(Vector3 pos, SplineSpawnData data, int index, int waveIndex, float zOffset, bool forPlanet) {
		Vector3 point = pos + data.points [index];
		point.x *= halfWidth;
		point.z += zOffset;

		if ((forPlanet ? selectedPlanetWave : selectedWave) == waveIndex) {
			BezierSpline.BezierControlPointMode mode = data.GetControlPointMode(index);
			Handles.color = SplineDebrisInspector.modeColors[(int)mode];
			float size = HandleUtility.GetHandleSize(point);
			if(index == 0) {
				size *= 2f;
			}
			Quaternion handleRotation = Quaternion.Euler(new Vector3());
			if(Handles.Button(point, handleRotation, size * 0.04f, size * 0.06f, Handles.DotCap)) {
				data.selectedIndex = index;
				if (forPlanet) {
					selectedWave = -1;
					selectedPlanetWave = waveIndex;
				} else {
					selectedPlanetWave = -1;
					selectedWave = waveIndex;
				}
				Repaint();
				EditorUtility.SetDirty (spawner);
			}

			if(data.selectedIndex == index) {
				EditorGUI.BeginChangeCheck();
				point = Handles.DoPositionHandle(point, handleRotation);
				if (EditorGUI.EndChangeCheck()) {
					point -= pos;
					point.y = 0;
					point.x /= halfWidth;
					point.z -= zOffset;
					data.points [index] = point;
					if (forPlanet) {
						selectedWave = -1;
						selectedPlanetWave = waveIndex;
					} else {
						selectedPlanetWave = -1;
						selectedWave = waveIndex;
					}
					EditorUtility.SetDirty (spawner);
					Repaint ();
				}
			}
		}

		return point;
	}

	private void drawRockAtPosition(Vector3 pos, float scale){
		Handles.color = GetColorForSpawnType(SpawnType.ROCK);
		Handles.DrawSolidDisc(pos, Vector3.up, scale * 0.5f);
		Handles.color = Color.black;
		Handles.DrawWireDisc (pos, Vector3.up, scale * 0.5f);
	}

	private void drawCrystalAtPosition(Vector3 pos, Color color){
		Handles.color = color;
		Handles.DrawWireCube (pos, new Vector3(1f, 1f, 2f));
	}

	private void drawDustAtPosition(Vector3 pos){
		Handles.color = GetColorForSpawnType(SpawnType.DUST);
		Handles.DrawWireDisc (pos, Vector3.up, 1f);
	}

	private void drawRadiationAtPosition(Vector3 pos){
		Handles.color = GetColorForSpawnType(SpawnType.RADIATION);
		Handles.DrawWireArc (pos, Vector3.up, Vector3.right, 180, 1f);
	}

	private void drawPrefabAtPosition(Vector3 pos, GameObject prefab, float scale){
		if (prefab != null) {
			if (prefab.name.Equals ("ParabolaRockWide")) {
				Handles.color = Color.grey;
				pos.z += 5f;
				Handles.DrawWireArc (pos, Vector3.up, Vector3.right, 180, 5f);
			} else if (prefab.name.Equals ("ParabolaRockNarrow")) {
				Handles.color = Color.grey;
				pos.z += 3f;
				Handles.DrawWireArc (pos, Vector3.up, Vector3.right, 180, 3f);
			} else if (prefab.name.Equals ("RockSpermLeft")) {
				Handles.color = Color.grey;
				Vector3 startTan = pos;
				startTan.x += 2f;
				startTan.z += 2.5f;
				Vector3 endTan = pos;
				endTan.x -= 2f;
				endTan.z += 2.5f;
				Vector3 end = pos;
				end.z += 5f;
				Handles.DrawBezier (pos, end, startTan, endTan, Color.grey, null, 3f);
			} else if (prefab.name.Equals ("RockSpermRight")) {
				Handles.color = Color.grey;
				Vector3 startTan = pos;
				startTan.x -= 2f;
				startTan.z += 2.5f;
				Vector3 endTan = pos;
				endTan.x += 2f;
				endTan.z += 2.5f;
				Vector3 end = pos;
				end.z += 5f;
				Handles.DrawBezier (pos, end, startTan, endTan, Color.grey, null, 3f);
			} else if (prefab.name.Equals ("Rock")) {
				drawRockAtPosition (pos, scale);
			} else if (prefab.name.Equals ("Crystal")) {
				PhotonColor randomColor = (PhotonColor)(Random.Range ((int)PhotonColor.ULTRAVIOLET, (int)PhotonColor.INFRARED));
				drawCrystalAtPosition (pos, randomColor.GetColor ());
			} else if (prefab.name.Equals ("Dust")) {
				drawDustAtPosition (pos);
			} else if (prefab.name.Equals ("RadWave")) {
				drawRadiationAtPosition (pos);
			} else if (prefab.name.Equals ("Dancers") || prefab.name.Equals("DustyDancers")) {
				drawDancersAtPosition (pos, scale);
			} else if (prefab.name.Equals("DancerGroup") || prefab.name.Equals("DustyDancerGroup")){
				drawDancersAtPosition (pos + new Vector3 (2f * scale, 0, 0), scale);
				drawDancersAtPosition (pos + new Vector3 (-1f * scale, 0, -1.732f * scale), scale);
				drawDancersAtPosition (pos + new Vector3 (-1f * scale, 0, 1.732f * scale), scale);
			} else {
				Handles.color = Color.cyan;
				Handles.DrawWireCube (pos, new Vector3(3f, 1f, 2f));
			}	
		}
	}

	private void drawDancersAtPosition(Vector3 pos, float scale){
		Vector3 right = pos;
		right.x += 0.75f * scale;
		drawRockAtPosition (right, scale);

		Vector3 left = pos;
		left.x -= 0.75f * scale;
		drawRockAtPosition (left, scale);
	}
	
	private Color GetColorForSpawnType(SpawnType type){
		Color retval = Color.white;
		switch(type){
			case SpawnType.CRYSTAL:
				retval = Color.blue;
				break;
			case SpawnType.DUST:
				retval = Color.white;
				break;
			case SpawnType.RADIATION:
				retval = Color.yellow;
				break;
			case SpawnType.ROCK:
				retval = Color.gray;
				break;
		}
		
		return retval;
	}

	/**
	 *
	 * INSPECTOR 
	 *
	 **/
	public override void OnInspectorGUI() {
		spawner = target as DebrisSpawner;

		EditorGUI.BeginChangeCheck();
		StaticFilter staticFilter = (StaticFilter)EditorGUILayout.ObjectField("Static Filter", spawner.staticFilter, typeof(StaticFilter), true);
		if (EditorGUI.EndChangeCheck()) {
			spawner.staticFilter = staticFilter;
			EditorUtility.SetDirty(spawner);
			Repaint();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject deathEffectPrefab = (GameObject)EditorGUILayout.ObjectField ("Death Effect Prefab", spawner.deathEffectPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.deathEffectPrefab = deathEffectPrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject scoreAwardPrefab = (GameObject)EditorGUILayout.ObjectField ("Score Award Prefab", spawner.scoreAwardPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.scoreAwardPrefab = scoreAwardPrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject debrisPrefab = (GameObject)EditorGUILayout.ObjectField ("Debris Prefab", spawner.debrisPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.debrisPrefab = debrisPrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject crystalPrefab = (GameObject)EditorGUILayout.ObjectField ("Crystal Prefab", spawner.transparentPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.transparentPrefab = crystalPrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject dustPrefab = (GameObject)EditorGUILayout.ObjectField ("Dust Prefab", spawner.dustPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.dustPrefab = dustPrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject radiationPrefab = (GameObject)EditorGUILayout.ObjectField ("Radiation Prefab", spawner.radiationPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.radiationPrefab = radiationPrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject splinePrefab = (GameObject)EditorGUILayout.ObjectField ("Spline Prefab", spawner.splinePrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.splinePrefab = splinePrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		GameObject rockTetraPrefab = (GameObject)EditorGUILayout.ObjectField ("Rock Tetra Prefab", spawner.rockTetraPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.rockTetraPrefab = rockTetraPrefab;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck ();
		RectTransform playerUI = (RectTransform)EditorGUILayout.ObjectField ("Player UI", spawner.playerUI, typeof(RectTransform), true);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.playerUI = playerUI;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		int waveSelection = EditorGUILayout.IntField("Selected Wave", selectedWave);
		if (EditorGUI.EndChangeCheck()) {
			if(waveSelection < waves.Count && waveSelection >= 0){
				selectedWave = waveSelection;
			}
			Repaint();
		}

    	if(GUILayout.Button("Add Wave")) {
            Undo.RecordObject(spawner, "Add Wave");
			Wave wave = new Wave ();
			if (spawner.waves.Count > 0) {
				Wave current = spawner.waves [spawner.waves.Count - 1];
				wave.timeTrigger = current.timeTrigger;
				wave.waveType = current.waveType;
				switch (wave.waveType) {
				case WaveType.FIVE_TIMES_FIVE_ROCK:
					wave.timeTrigger += 5f;
					break;

				case WaveType.PREFAB_RAIN:
					wave.duration = current.duration;
					wave.timeTrigger += current.duration;
					wave.frequency = current.frequency;
					break;

				case WaveType.FIVE_OF_PREFAB:
					wave.timeTrigger += 3f;
					wave.prefab = current.prefab;
					break;

				case WaveType.FIVE_CRYSTAL:
					wave.timeTrigger += 3f;
					break;

				case WaveType.ROCK_HELIX:
					wave.timeTrigger += 3f;
					break;

				case WaveType.SPLINE:
					wave.timeTrigger += 5f;
					wave.splineData = current.splineData.clone();
					break;

				case WaveType.MANUAL:
					wave.timeTrigger += current.spawns.Count;
					break;
				}
			} else {
				wave.timeTrigger++;
				if (wave.waveType == WaveType.PREFAB_RAIN) {
					wave.duration = 5f;
				}
			}
			spawner.waves.Add (wave);
			selectedWave = spawner.waves.Count - 1;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		if (selectedWave > -1) {
			if (GUILayout.Button ("Duplicate Wave")) {
				Undo.RecordObject (spawner, "Duplicate Wave");
				Wave wave = new Wave ();
				Wave current = spawner.waves [selectedWave];
				wave.timeTrigger = spawner.waves[spawner.waves.Count-1].timeTrigger;
				wave.waveType = current.waveType;
				switch (wave.waveType) {
					case WaveType.FIVE_TIMES_FIVE_ROCK:
						wave.timeTrigger += 5f;
						break;

					case WaveType.PREFAB_RAIN:
						wave.duration = current.duration;
						wave.timeTrigger += current.duration;
						wave.frequency = current.frequency;
						break;

					case WaveType.FIVE_OF_PREFAB:
						wave.timeTrigger += 3f;
						wave.prefab = current.prefab;
						break;

					case WaveType.FIVE_CRYSTAL:
						wave.timeTrigger += 3f;
						break;

					case WaveType.ROCK_HELIX:
						wave.timeTrigger += 3f;
						break;

					case WaveType.SPLINE:
						wave.timeTrigger += 5f;
						wave.splineData = current.splineData.clone ();
						break;

					case WaveType.MANUAL:
						wave.timeTrigger += current.spawns.Count;
						break;
				}
				wave.spawns = new List<Spawn> ();

				for (int i = 0; i < current.spawns.Count; i++) {
					wave.spawns.Add(current.spawns [i].Clone ());
				}

				spawner.waves.Add (wave);
				selectedWave = spawner.waves.Count - 1;
				EditorUtility.SetDirty (spawner);
				Repaint ();
			}
		}

		if (selectedWave > -1 && GUILayout.Button ("Delete Selected Wave")) {
			waves.RemoveAt (selectedWave);
			if (waves.Count <= selectedWave) {
				selectedWave--;
			}
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		if (selectedSpawn > -1 && selectedWave > -1 && GUILayout.Button ("Delete Selected Spawn")) {
			waves [selectedWave].spawns.RemoveAt (selectedSpawn);
			Undo.RecordObject (spawner, "Delete Spawn");
			if (waves [selectedWave].spawns.Count <= selectedSpawn) {
				selectedSpawn--;
			}
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		waveInspector();

		EditorGUI.BeginChangeCheck ();
		GameObject planetPrefab = (GameObject)EditorGUILayout.ObjectField ("Planet Prefab", spawner.planetPrefab, typeof(GameObject), false);
		if (EditorGUI.EndChangeCheck ()) {
			spawner.planetPrefab = planetPrefab;
			EditorUtility.SetDirty(spawner);
		}

		EditorGUI.BeginChangeCheck();
		int planetWaveSelection = EditorGUILayout.IntField("Selected Planet Wave", selectedPlanetWave);
		if (EditorGUI.EndChangeCheck()) {
			if(planetWaveSelection < planetWaves.Count && planetWaveSelection >= 0){
				selectedPlanetWave = planetWaveSelection;
				Debug.Log ("selectedPlanetWave = " + selectedPlanetWave);
				selectedWave = -1;
			}
			Repaint();
			EditorUtility.SetDirty(spawner);
		}

		if(GUILayout.Button("Add Planet Wave")) {
			Undo.RecordObject(spawner, "Add Planet Wave");
			Wave wave = new Wave ();
			if (spawner.planetWaves.Count > 0) {
				Wave current = spawner.planetWaves [spawner.planetWaves.Count - 1];
				wave.timeTrigger = current.timeTrigger;
				wave.waveType = current.waveType;
				switch (wave.waveType) {
				case WaveType.FIVE_TIMES_FIVE_ROCK:
					wave.timeTrigger += 5f;
					break;

				case WaveType.PREFAB_RAIN:
					wave.duration = current.duration;
					wave.timeTrigger += current.duration;
					wave.frequency = current.frequency;
					break;

				case WaveType.FIVE_OF_PREFAB:
					wave.timeTrigger += 3f;
					wave.prefab = current.prefab;
					break;

				case WaveType.FIVE_CRYSTAL:
					wave.timeTrigger += 3f;
					break;

				case WaveType.ROCK_HELIX:
					wave.timeTrigger += 3f;
					break;

				case WaveType.SPLINE:
					wave.timeTrigger += 5f;
					wave.splineData = current.splineData.clone();
					break;

				case WaveType.MANUAL:
					wave.timeTrigger += current.spawns.Count;
					break;
				}
			} else {
				wave.timeTrigger++;
				if (wave.waveType == WaveType.PREFAB_RAIN) {
					wave.duration = 5f;
				}
			}
			spawner.planetWaves.Add (wave);
			selectedPlanetWave = spawner.planetWaves.Count - 1;
			selectedWave = -1;
			EditorUtility.SetDirty(spawner);
			Repaint ();
		}

		if (selectedPlanetWave > -1) {
			if (GUILayout.Button ("Duplicate Planet Wave")) {
				Undo.RecordObject (spawner, "Duplicate Wave");
				Wave wave = new Wave ();
				Wave current = spawner.planetWaves [selectedPlanetWave];
				wave.timeTrigger = spawner.planetWaves[spawner.planetWaves.Count-1].timeTrigger;
				wave.waveType = current.waveType;
				switch (wave.waveType) {
				case WaveType.FIVE_TIMES_FIVE_ROCK:
					wave.timeTrigger += 5f;
					break;

				case WaveType.PREFAB_RAIN:
					wave.duration = current.duration;
					wave.timeTrigger += current.duration;
					wave.frequency = current.frequency;
					break;

				case WaveType.FIVE_OF_PREFAB:
					wave.timeTrigger += 3f;
					wave.prefab = current.prefab;
					break;

				case WaveType.FIVE_CRYSTAL:
					wave.timeTrigger += 3f;
					break;

				case WaveType.ROCK_HELIX:
					wave.timeTrigger += 3f;
					break;

				case WaveType.SPLINE:
					wave.timeTrigger += 5f;
					wave.splineData = current.splineData.clone ();
					break;

				case WaveType.MANUAL:
					wave.timeTrigger += current.spawns.Count;
					break;
				}
				wave.spawns = new List<Spawn> ();

				for (int i = 0; i < current.spawns.Count; i++) {
					wave.spawns.Add(current.spawns [i].Clone ());
				}

				spawner.planetWaves.Add (wave);
				selectedPlanetWave = spawner.planetWaves.Count - 1;
				EditorUtility.SetDirty (spawner);
				Repaint ();
			}
		}

		if (selectedPlanetWave > -1 && GUILayout.Button ("Delete Selected Planet Wave")) {
			planetWaves.RemoveAt (selectedPlanetWave);
			if (planetWaves.Count <= selectedPlanetWave) {
				selectedPlanetWave--;
			}
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}


		if (selectedSpawn > -1 && selectedPlanetWave > -1 && GUILayout.Button ("Delete Selected Planet Spawn")) {
			planetWaves [selectedPlanetWave].spawns.RemoveAt (selectedSpawn);
			Undo.RecordObject (spawner, "Delete Planet Spawn");
			if (planetWaves [selectedPlanetWave].spawns.Count <= selectedSpawn) {
				selectedSpawn--;
			}
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		planetWaveInspector ();
    }

	private void waveInspector(){
		if(selectedWave >= 0){
        	Wave wave = waves[selectedWave];
	        EditorGUI.BeginChangeCheck();
	        WaveType type = (WaveType)EditorGUILayout.EnumPopup("Wave type", wave.waveType);
	        if(EditorGUI.EndChangeCheck()){
	        	Undo.RecordObject(spawner, "Change wave type");
	        	wave.waveType = type;
				EditorUtility.SetDirty(spawner);
				Repaint ();
	        }

			GameObject prefab;
	        switch(wave.waveType){
	        	case WaveType.MANUAL:
			        if(waves[selectedWave].spawns.Count > 0){
			        	spawnInspector(false);
			        }

			        if(GUILayout.Button("Add Spawn")){
			        	Undo.RecordObject(spawner, "Add Spawn");
			        	Spawn spawn = new Spawn();
			        	spawn.position = new Vector3();
						if(wave.spawns.Count > 0){
							Spawn lastSpawn = wave.spawns[wave.spawns.Count - 1];
			        		spawn.color = lastSpawn.color;
							spawn.prefab = lastSpawn.prefab;
			        		spawn.scale = lastSpawn.scale;
			        		spawn.type = lastSpawn.type;
			        		spawn.velocity = lastSpawn.velocity;
							spawn.position = lastSpawn.position;
							spawn.maxHealth = lastSpawn.maxHealth;
			        	}

						wave.spawns.Add(spawn);
						selectedSpawn = wave.spawns.Count - 1;
	            		EditorUtility.SetDirty(spawner);
	            		Repaint();
			        }
	        		break;

	        	case WaveType.FIVE_CRYSTAL:
	        		//TODO color picker makes sense
	        		break;

				case WaveType.FIVE_OF_PREFAB:
					EditorGUI.BeginChangeCheck ();
					prefab = (GameObject)EditorGUILayout.ObjectField ("Prefab", wave.prefab, typeof(GameObject), false);
					if (EditorGUI.EndChangeCheck ()) {
						Undo.RecordObject (spawner, "Change wave prefab");
						wave.prefab = prefab;
						EditorUtility.SetDirty (spawner);
					}

					EditorGUI.BeginChangeCheck ();
					float frequency = EditorGUILayout.FloatField ("Spacing", wave.frequency);
					if (EditorGUI.EndChangeCheck ()) {
						Undo.RecordObject (spawner, "Change Wave Spacing");
						wave.frequency = frequency;
						EditorUtility.SetDirty (spawner);
						Repaint ();
					}
	        		break;

        		case WaveType.FIVE_TIMES_FIVE_ROCK:
        			//TODO anything?
        			break;

				case WaveType.PREFAB_RAIN:
					EditorGUI.BeginChangeCheck ();
					prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", wave.prefab, typeof(GameObject), false);
					if(EditorGUI.EndChangeCheck()){
						Undo.RecordObject(spawner, "Change Wave Prefab");
						wave.prefab = prefab;
						EditorUtility.SetDirty (spawner);
						Repaint ();
					}
				
					EditorGUI.BeginChangeCheck ();
					float duration = EditorGUILayout.FloatField ("Duration", wave.duration);
					if (EditorGUI.EndChangeCheck ()) {
						Undo.RecordObject (spawner, "Change Wave Duration");
						float diff = duration - wave.duration;
						wave.duration = duration;
						for (int i = selectedWave + 1; i < spawner.waves.Count; i++) {
							spawner.waves [i].timeTrigger += diff;
						}
						EditorUtility.SetDirty (spawner);
						Repaint ();
					}

					EditorGUI.BeginChangeCheck ();
					frequency = EditorGUILayout.FloatField ("Frequency", wave.frequency);
					if (EditorGUI.EndChangeCheck ()) {
						Undo.RecordObject (spawner, "Change Wave Frequency");
						wave.frequency = frequency;
						EditorUtility.SetDirty (spawner);
						Repaint ();
					}
    				break;

				case WaveType.ROCK_HELIX:
					EditorGUI.BeginChangeCheck ();
					float iterations = EditorGUILayout.FloatField ("Iterations", wave.duration);
					if (EditorGUI.EndChangeCheck ()) {
						Undo.RecordObject (spawner, "Change Helix Iterations");
						wave.duration = iterations;
						EditorUtility.SetDirty (spawner);

						Repaint ();
					}

					EditorGUI.BeginChangeCheck ();
					frequency = EditorGUILayout.FloatField ("Frequency", wave.frequency);
					if (EditorGUI.EndChangeCheck ()) {
						Undo.RecordObject (spawner, "Change Wave Frequency");
						wave.frequency = frequency;
						EditorUtility.SetDirty (spawner);

						Repaint ();
					}
					break;

				case WaveType.SPLINE:
					drawSplineInspector (wave.splineData);
					break;

	        }
		}
    }


	private void planetWaveInspector(){
		if(selectedPlanetWave >= 0 && planetWaves.Count > selectedPlanetWave){
			Wave wave = planetWaves[selectedPlanetWave];
			EditorGUI.BeginChangeCheck();
			WaveType type = (WaveType)EditorGUILayout.EnumPopup("Planet Wave type", wave.waveType);
			if(EditorGUI.EndChangeCheck()){
				Undo.RecordObject(spawner, "Change planet wave type");
				wave.waveType = type;
				EditorUtility.SetDirty(spawner);
				Repaint ();
			}

			GameObject prefab;
			switch(wave.waveType){
			case WaveType.MANUAL:
				if(planetWaves[selectedPlanetWave].spawns.Count > 0){
					spawnInspector(true);
				}

				if(GUILayout.Button("Add Spawn")){
					Undo.RecordObject(spawner, "Add Spawn");
					Spawn spawn = new Spawn();
					spawn.position = new Vector3();
					if(wave.spawns.Count > 0){
						Spawn lastSpawn = wave.spawns[wave.spawns.Count - 1];
						spawn.color = lastSpawn.color;
						spawn.prefab = lastSpawn.prefab;
						spawn.scale = lastSpawn.scale;
						spawn.type = lastSpawn.type;
						spawn.velocity = lastSpawn.velocity;
						spawn.position = lastSpawn.position;
						spawn.maxHealth = lastSpawn.maxHealth;
					}

					wave.spawns.Add(spawn);
					selectedSpawn = wave.spawns.Count - 1;
					EditorUtility.SetDirty(spawner);
					Repaint();
				}
				break;

			case WaveType.FIVE_CRYSTAL:
				//TODO color picker makes sense
				break;

			case WaveType.FIVE_OF_PREFAB:
				EditorGUI.BeginChangeCheck ();
				prefab = (GameObject)EditorGUILayout.ObjectField ("Prefab", wave.prefab, typeof(GameObject), false);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Change wave prefab");
					wave.prefab = prefab;
					EditorUtility.SetDirty (spawner);
				}

				EditorGUI.BeginChangeCheck ();
				float frequency = EditorGUILayout.FloatField ("Spacing", wave.frequency);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Change Wave Spacing");
					wave.frequency = frequency;
					EditorUtility.SetDirty (spawner);
					Repaint ();
				}
				break;

			case WaveType.FIVE_TIMES_FIVE_ROCK:
				//TODO anything?
				break;

			case WaveType.PREFAB_RAIN:
				EditorGUI.BeginChangeCheck ();
				prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", wave.prefab, typeof(GameObject), false);
				if(EditorGUI.EndChangeCheck()){
					Undo.RecordObject(spawner, "Change Wave Prefab");
					wave.prefab = prefab;
					EditorUtility.SetDirty (spawner);
					Repaint ();
				}

				EditorGUI.BeginChangeCheck ();
				float duration = EditorGUILayout.FloatField ("Duration", wave.duration);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Change Wave Duration");
					float diff = duration - wave.duration;
					wave.duration = duration;
					for (int i = selectedWave + 1; i < spawner.waves.Count; i++) {
						spawner.planetWaves [i].timeTrigger += diff;
					}
					EditorUtility.SetDirty (spawner);
					Repaint ();
				}

				EditorGUI.BeginChangeCheck ();
				frequency = EditorGUILayout.FloatField ("Frequency", wave.frequency);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Change Wave Frequency");
					wave.frequency = frequency;
					EditorUtility.SetDirty (spawner);
					Repaint ();
				}
				break;

			case WaveType.ROCK_HELIX:
				EditorGUI.BeginChangeCheck ();
				float iterations = EditorGUILayout.FloatField ("Iterations", wave.duration);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Change Helix Iterations");
					wave.duration = iterations;
					EditorUtility.SetDirty (spawner);

					Repaint ();
				}

				EditorGUI.BeginChangeCheck ();
				frequency = EditorGUILayout.FloatField ("Frequency", wave.frequency);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Change Wave Frequency");
					wave.frequency = frequency;
					EditorUtility.SetDirty (spawner);

					Repaint ();
				}
				break;

			case WaveType.SPLINE:
				drawSplineInspector (wave.splineData);
				break;

			}
		}
	}

	private void spawnInspector(bool forPlanet){
        if(selectedSpawn >= 0){
			EditorGUI.BeginChangeCheck();
	        int spawnSelection = EditorGUILayout.IntField("Selected Spawn", selectedSpawn);
	        if(EditorGUI.EndChangeCheck()){
				int count = forPlanet? planetWaves[selectedPlanetWave].spawns.Count : waves[selectedWave].spawns.Count;
	        	if(spawnSelection < count && spawnSelection >= 0){
	        		selectedSpawn = spawnSelection;
	        	}
	        	Repaint();
	        }

			Spawn spawn = forPlanet ? planetWaves[selectedPlanetWave].spawns[selectedSpawn] :
				waves[selectedWave].spawns[selectedSpawn];

	        EditorGUI.BeginChangeCheck();
	        Vector3 position = EditorGUILayout.Vector3Field("Position", spawn.position);
			if(EditorGUI.EndChangeCheck()){
	        	Undo.RecordObject(spawner, "Change Spawn Position");
	        	position.y = 0;
				spawn.position = position;
				EditorUtility.SetDirty(spawner);
				Repaint ();
			}

			EditorGUI.BeginChangeCheck ();
			Vector3 velocity = EditorGUILayout.Vector3Field ("Velocity", spawn.velocity);
			if (EditorGUI.EndChangeCheck ()) {
				Undo.RecordObject (spawner, "Change Velocity");
				spawn.velocity = velocity;
				EditorUtility.SetDirty (spawner);
				Repaint ();
			}

			EditorGUI.BeginChangeCheck ();
			float scale = EditorGUILayout.FloatField ("Scale", spawn.scale);
			if (EditorGUI.EndChangeCheck ()) {
				Undo.RecordObject (spawner, "Change Scale");
				spawn.scale = scale;
				EditorUtility.SetDirty (spawner);
				Repaint ();
			}

			EditorGUI.BeginChangeCheck ();
			int maxHealth = EditorGUILayout.IntField ("Max Health", spawn.maxHealth);
			if (EditorGUI.EndChangeCheck ()) {
				Undo.RecordObject (spawner, "Change Max Health");
				spawn.maxHealth = maxHealth;
				EditorUtility.SetDirty (spawner);
				Repaint ();
			}

	        EditorGUI.BeginChangeCheck();
	        SpawnType type = (SpawnType)EditorGUILayout.EnumPopup("Type", spawn.type);
			if(EditorGUI.EndChangeCheck()){
	        	Undo.RecordObject(spawner, "Change Spawn Type");
				spawn.type = type;
				EditorUtility.SetDirty(spawner);
				Repaint ();
	        }

			if (type == SpawnType.PREFAB) {
				EditorGUI.BeginChangeCheck ();
				GameObject prefab = (GameObject)EditorGUILayout.ObjectField ("Prefab", spawn.prefab, typeof(GameObject), false);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Spawn prefab change");
					spawn.prefab = prefab;

					ObjectMover mover = prefab.GetComponent<ObjectMover>();
					if (mover != null) {
						spawn.velocity = mover.velocity;
					}

					Destructable destructable = prefab.GetComponent<Destructable>();
					if(destructable != null) {
						spawn.maxHealth = destructable.maxHealth;
					}

					EditorUtility.SetDirty (spawner);
					Repaint ();
				}
			} else if (type == SpawnType.CRYSTAL) {
				EditorGUI.BeginChangeCheck ();
				PhotonColor color = (PhotonColor)EditorGUILayout.EnumPopup ("Color", spawn.color);
				if (EditorGUI.EndChangeCheck ()) {
					Undo.RecordObject (spawner, "Change Crystal Color");
					spawn.color = color;
					EditorUtility.SetDirty (spawner);
					Repaint ();
				}
			}else if (type == SpawnType.SPLINE) {
				drawSplineInspector (spawn.splineData);
			}
        }
    }

	private void drawSplineInspector(SplineSpawnData splineData) {
		EditorGUI.BeginChangeCheck();
		int frequency = EditorGUILayout.IntField("Frequency", splineData.frequency);
		if (EditorGUI.EndChangeCheck()) {
			Undo.RecordObject (spawner, "Change spline frequency");
			splineData.frequency = frequency;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		float scaleMin = EditorGUILayout.FloatField("Scale Min", splineData.scaleMin);
		if (EditorGUI.EndChangeCheck()) {
			splineData.scaleMin = scaleMin;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		float scaleMax = EditorGUILayout.FloatField("Scale Max", splineData.scaleMax);
		if (EditorGUI.EndChangeCheck()) {
			splineData.scaleMax = scaleMax;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		bool lookForward = EditorGUILayout.Toggle("Look Forward", splineData.lookingForward);
		if (EditorGUI.EndChangeCheck()) {
			splineData.lookingForward = lookForward;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		int maxHealth = EditorGUILayout.IntField("Max Health Per Spawn", splineData.maxHealth);
		if (EditorGUI.EndChangeCheck()) {
			splineData.maxHealth = maxHealth;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		Vector3 velocity = EditorGUILayout.Vector3Field("Spawn Velocity", splineData.velocity);
		if (EditorGUI.EndChangeCheck()) {
			splineData.velocity = velocity;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		PhotonEnum.PhotonColor color = (PhotonEnum.PhotonColor)EditorGUILayout.EnumPopup("Color", splineData.color);
		if(EditorGUI.EndChangeCheck()){
			splineData.color = color;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		SplineDebrisCreator.SplineType type = (SplineDebrisCreator.SplineType)EditorGUILayout.EnumPopup("Type", splineData.type);
		if (EditorGUI.EndChangeCheck()) {
			splineData.type = type;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		EditorGUI.BeginChangeCheck();
		bool loop = EditorGUILayout.Toggle("Loop", splineData.loop);
		if (EditorGUI.EndChangeCheck()) {
			splineData.loop = loop;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		if(splineData.selectedIndex >= 0 && splineData.selectedIndex < splineData.points.Length) {
			DrawSelectedPointInspector(splineData);
		}

		if(GUILayout.Button("Add Curve")) {
			splineData.AddCurve();
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}

		if(GUILayout.Button("Remove Curve")) {
			splineData.RemoveCurve();
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}
	}

	private void DrawSelectedPointInspector(SplineSpawnData data ) {
		GUILayout.Label("Selected Point");
		EditorGUI.BeginChangeCheck();
		Vector3 point = EditorGUILayout.Vector3Field("Position", data.points[data.selectedIndex]);
		if(EditorGUI.EndChangeCheck()) {
			data.points [data.selectedIndex] = point;
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}
		EditorGUI.BeginChangeCheck();
		BezierSpline.BezierControlPointMode mode = (BezierSpline.BezierControlPointMode)EditorGUILayout.EnumPopup("Mode",data.GetControlPointMode(data.selectedIndex));
		if (EditorGUI.EndChangeCheck()) {
			data.SetControlPointMode (data.selectedIndex, mode);
			EditorUtility.SetDirty (spawner);
			Repaint ();
		}
	}

    private void SortWaves() {
        //Making the assumption that only one wave will be out of order at a time
        for(int i = 0; i<spawner.waves.Count - 1; i++)
        {
            Wave current = spawner.waves[i];
            Wave next = spawner.waves[i + 1];
            if(current.timeTrigger > next.timeTrigger) {
                spawner.waves[i] = next;
                spawner.waves[i + 1] = current;

				for (int j = i + 1; j < spawner.waves.Count; j++) {
					spawner.waves[j].timeTrigger += 5f;
				}

				if (selectedWave == i)
                {
                    selectedWave = i + 1;
                }else if(selectedWave == i + 1)
                {
                    selectedWave = i;
                }
            }
        }

        for (int i = 0; i < spawner.planetWaves.Count - 1; i++)
        {
            Wave current = spawner.planetWaves[i];
            Wave next = spawner.planetWaves[i + 1];
            if (current.timeTrigger > next.timeTrigger) {
                spawner.planetWaves[i] = next;
                spawner.planetWaves[i + 1] = current;

				for(int j = i + 1; j < spawner.planetWaves.Count; j++) {
					spawner.planetWaves[j].timeTrigger += 5f;
				}
            }
        }
    }
}
#endif