﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticFilter : MonoBehaviour {
	public Material material;
	public float percent = 0;

	public float baseline = 0f;
	public float baselineIncrement = 0.01f;
	public float spikeDiff = 0.8f;
	public float spikeDuration = 0.01f;

	void Start() {
		percent = 0;
		material.SetFloat("_Percent", percent);
	}

	public void Spike() {
		baseline += baselineIncrement;
		percent = baseline + spikeDiff;
		StopAllCoroutines();
		StartCoroutine(SpikeTime());
	}

	public void FullStatic() {
		StopAllCoroutines();
		percent = 1f;
	}

	private IEnumerator SpikeTime() {
		yield return new WaitForSeconds(spikeDuration);
		percent = baseline;
	}

	void OnRenderImage(RenderTexture src, RenderTexture dest) {
		material.SetFloat("_Percent", percent);
		Graphics.Blit(src, dest, material);
	}
}
