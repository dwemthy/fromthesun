﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthController : PlanetBehavior {
    public Orbiter moonPrefab;
	public StaticFilter staticFilter;
	public GameObject uiPanel;

	public override void Start ()
	{
		base.Start ();
        LaunchMoon();
	}

	public override void Damage(int damage, Vector3 location) {
		base.Damage(damage, location);

		if(staticFilter != null) {
			staticFilter.Spike();
		}
	}

	public override void Die() {
		base.Die();

		if(staticFilter != null) {
			staticFilter.FullStatic();
		}

		if(uiPanel != null) {
			uiPanel.SetActive(false);
		}
	}

	private void LaunchMoon(){
        if(moonPrefab != null)
        {
            Orbiter moon = Instantiate(moonPrefab);
            moon.MarkLaunched();
            moon.currentAngle = 0f;
            moon.orbitTarget = transform;
        }
    }
}