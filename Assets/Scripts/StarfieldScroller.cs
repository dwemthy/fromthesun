﻿using UnityEngine;
using System.Collections;

public class StarfieldScroller : MonoBehaviour {
	public float PerSecond = 0.15f;
	public float Parralax = 0.01f;

	private Vector3 lastPlayerLocation;

	// Update is called once per frame
	void Update () {
		MeshRenderer mr = GetComponent<MeshRenderer>();
		Material mat = mr.material;
		Vector2 offset = mat.mainTextureOffset;

		offset.y += LevelManager.ScaledTime() * PerSecond;

		if(!lastPlayerLocation.Equals(new Vector3())){
			Vector3 playerLocation = DataCenter.Instance.PlayerPosition;
			float xDiff = playerLocation.x - lastPlayerLocation.x;
			offset.x += xDiff * Parralax;
		}

		lastPlayerLocation = DataCenter.Instance.PlayerPosition;

		mat.mainTextureOffset = offset;
	}
}