﻿using UnityEngine;

public class TargetShooter : PoweredShooter {
	private Vector3 toTarget;

	public void SetToTarget(Vector3 target) {
		toTarget = target;
	}

    protected override void shootVelocity(ObjectMover oMover)
    {
        float magnitude = oMover.velocity.magnitude;
		Vector3 velocity = toTarget.normalized * magnitude;
		oMover.SetVelocity(velocity);
    }
}