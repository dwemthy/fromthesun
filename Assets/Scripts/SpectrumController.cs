﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(RectTransform))]
public class SpectrumController : MonoBehaviour {
	public int sizeX, sizeY;
	public float animationTime = 0.1f;
	public RectTransform playerUI;

	private float timeAnimating;
	private bool animatingUp = false;
	
	private Vector3[] vertices;
	private int[] triangles;
	private Vector2[] uvs;
	private RectTransform rect;
	private Mesh mesh;
	private PhotonEnum.PhotonColor color;

	private float littleSize = 0.2f;
	private float bigSize = 0.8f;
	
	void Start(){
		rect = GetComponent<RectTransform>();
		timeAnimating = animationTime;

		Vector3 uiPos = playerUI.position;
		uiPos = Camera.main.ScreenToWorldPoint(uiPos);
		uiPos.y = transform.position.y;

		//Size to fit camera
        float uiWidth = playerUI.rect.width;
        uiWidth = Mathf.Abs(Camera.main.ScreenToWorldPoint(new Vector3(uiWidth, 0, 0)).x)*0.5f;

		float camHeight = 2f * Camera.main.orthographicSize;

		SetRect(uiPos, uiWidth, camHeight);
	}

	void Update(){
		PhotonEnum.PhotonColor playerColor = DataCenter.Instance.PlayerColor;
		if(color != playerColor){
			animatingUp = (color - playerColor) < 0;
			color = playerColor;
			timeAnimating = 0;
		}

		if(timeAnimating < animationTime){
			timeAnimating += LevelManager.ScaledTime();
			Regenerate();
		}
	}

	public void SetRect(Vector3 position, float width, float height){
		transform.position = position;
		rect.sizeDelta = new Vector2(width, height);
		Regenerate();
	}

	private void Regenerate(){
		GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Spectrum Mesh";
		
		Generate();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
	}
	
	private void Generate(){
		int bigGapAfter = (int)PhotonEnum.PhotonColor.INFRARED - (int)color;

		float xDelta = rect.rect.width / sizeX;
		float littleY = rect.rect.height * littleSize * (1f/sizeY);
		float animationScale = timeAnimating/animationTime;
		if(animationScale > 1f){
			animationScale = 1;
		}

		float bigYGrow = rect.rect.height * bigSize * animationScale;
		float bigYShrink = rect.rect.height * bigSize * (1 - animationScale);
		float xAnchor = -rect.rect.width * 0.5f;
		float yAnchor = -rect.rect.height * 0.5f;
	
		vertices = new Vector3[(sizeX + 1) * (sizeY +1)];
		uvs = new Vector2[vertices.Length];
		float vertY = yAnchor;
		for(int i = 0, y = 0; y <= sizeY; y++){
			for(int x = 0; x <= sizeX; x++, i++){
				float vertX = xAnchor + (xDelta * x);
				vertices[i] = new Vector3(vertX,vertY);
			  	uvs[i] = new Vector2((float)x / sizeX, (float)y / sizeY);
			}

			if(animatingUp){
				if(y == bigGapAfter){
					vertY += bigYGrow;
				}else if(y == bigGapAfter + 1){
					vertY += bigYShrink;
				}
			}else{
				if(y == bigGapAfter - 1){
					vertY += bigYShrink;
				}else if(y == bigGapAfter){
					vertY += bigYGrow;
				}
			}

			vertY += littleY;
		}
		
		triangles = new int[sizeX * sizeY * 6];
		for(int ti = 0, vi = 0, y = 0; y < sizeY; y++, vi++) {
			for(int x = 0; x < sizeX; x++, ti += 6, vi++){
				triangles[ti] = vi;
				triangles[ti + 3] = triangles[ti + 2] = vi + 1;
				triangles[ti + 4] = triangles[ti + 1] = vi + sizeX + 1;
				triangles[ti + 5] = vi + sizeX + 2;
			}
		}
	}
}