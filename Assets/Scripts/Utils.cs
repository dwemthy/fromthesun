using UnityEngine;
using System.Collections.Generic;

public class Utils{
	
	public static List<T> Shuffle<T>(List<T> list){
		for(int i = 0; i < list.Count * 2; i++){
			int n = Random.Range(0, list.Count);
			int k = Random.Range(0, list.Count);
			
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
		
		return list;
	}
}