﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Collections.Generic;
using PhotonEnum;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter), typeof(RectTransform))]
public class ProgressFiller : MonoBehaviour {
	private Vector3[] vertices;
	private int[] triangles;
	private Vector2[] uvs;
	private RectTransform rect;
	private Mesh mesh;
	private PhotonEnum.PhotonColor color;
	private Material material;

	public RectTransform uiBounds;
	public float overFill = 0.1f;

	public GameObject progressThumb;

	private float fill = 0;
	private float height = 0;

	int sizeX = 1;

	List<Progress> progressList;
	Progress currentProgress;

	void Start(){
		fill = 0;
		progressList = new List<Progress> ();
		currentProgress = new Progress ();
		progressList.Add (currentProgress);

		rect = GetComponent<RectTransform>();
		material = GetComponent<MeshRenderer> ().material;

		Vector3 uiPos = uiBounds.position;
		uiPos = Camera.main.ScreenToWorldPoint(uiPos);
		uiPos.y = transform.position.y;

		//Size to fit camera
		Vector3 min = new Vector3(uiBounds.rect.min.x, uiBounds.rect.min.y, 0);
		Vector3 max = new Vector3 (uiBounds.rect.max.x, uiBounds.rect.max.y, 0);
		min = Camera.main.ScreenToWorldPoint (min);
		max = Camera.main.ScreenToWorldPoint (max);

		float uiWidth = max.x - min.x;
		float uiHeight = max.z - min.z;

		SetRect(uiPos, uiWidth, uiHeight);

		Vector2 newMin = rect.rect.min + new Vector2(transform.position.x, transform.position.z);
		Vector2 newMax = rect.rect.max + new Vector2(transform.position.x, transform.position.z);

		height = uiHeight;

		if (material != null) {
			material.SetFloat ("_MinX", newMin.x);
			material.SetFloat ("_MaxX", newMax.x);
			material.SetFloat ("_MaxY", newMin.y);
		}

		if (progressThumb != null) {
			Vector3 thumbPos = progressThumb.transform.position;
			thumbPos.z = transform.position.z + newMax.y;
			thumbPos.x = transform.position.x;
			progressThumb.transform.position = thumbPos;
		}
	}

	public void AddFill(float fillToAdd){
		currentProgress.amount += fillToAdd;
		fill += fillToAdd;

		PhotonEnum.PhotonColor playerColor = DataCenter.Instance.PlayerColor;
		if (playerColor != currentProgress.color) {
            IDictionary<string, object> eventParams = new Dictionary<string, object>();
            eventParams.Add(DataCenter.PARAM_COLOR_CHANGE_FROM, currentProgress.color.ColorName());
            eventParams.Add(DataCenter.PARAM_COLOR_DURATION, currentProgress.amount);
            eventParams.Add(DataCenter.PARAM_COLOR_CHANGE_TO, playerColor.ColorName());
            Analytics.CustomEvent(DataCenter.EVENT_COLOR_CHANGE, eventParams);
			currentProgress = new Progress ();
			currentProgress.color = playerColor;
			progressList.Add (currentProgress);
		}

		Regenerate ();

		float maxY = (rect.rect.min.y + transform.position.z) + ((fill + overFill) * height);
		if (material != null) {
			material.SetFloat ("_MaxY", maxY);
		}

		if (progressThumb != null) {
			Vector3 thumbPos = progressThumb.transform.position;
			thumbPos.z = maxY;
			progressThumb.transform.position = thumbPos;
		}
	}

	public void SetRect(Vector3 position, float width, float height){
		transform.position = position;
		rect.sizeDelta = new Vector2(width, height);
		Regenerate();
	}

	private void Regenerate(){
		GetComponent<MeshFilter>().mesh = mesh = new Mesh();
		mesh.name = "Spectrum Mesh";

		Generate();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		CanvasRenderer canvasRenderer = GetComponent<CanvasRenderer> ();
		canvasRenderer.SetMesh (mesh);
	}

	private void Generate(){
		float xAnchor = -rect.rect.width * 0.5f;
		float yAnchor = -rect.rect.height * 0.5f;

		float xDelta = rect.rect.width / sizeX;

		//4 vertices and uvs per progress item, plus one extra for the top of the mesh
		vertices = new Vector3[(progressList.Count + 1) * 4];
		uvs = new Vector2[vertices.Length];
		//2 triangles, 6 points, per progress, plus one extra for the top of the mesh
		triangles = new int[(progressList.Count + 1) * 6];

		float vertY = 0;
		float uvY;
		float progressAccumulation = 0;
		int vi = 0, uvi = 0, ti = 0;
		for (int pi = 0; pi < progressList.Count; pi++, ti+=6) {
			Progress progress = progressList [pi];
			//Triangles!
			triangles [ti] = vi;
			triangles [ti + 2] = triangles [ti + 3] = vi + 1;
			triangles [ti + 1] = triangles [ti + 4] = vi + 2;
			triangles [ti + 5] = vi + 3;

			//Vertices!
			vertY = yAnchor + progressAccumulation * rect.rect.height;
			vertices [vi] = new Vector3(xAnchor, vertY);
			vi++;
			vertices [vi] = new Vector3 (xAnchor + xDelta, vertY);
			vi++;
			progressAccumulation += progress.amount;
			vertY = yAnchor + progressAccumulation * rect.rect.height;
			vertices [vi] = new Vector3(xAnchor, vertY);
			vi++;
			vertices [vi] = new Vector3 (xAnchor + xDelta, vertY);
			vi++;

			//UVS!
			uvY = ((float)PhotonEnum.PhotonColor.INFRARED - (float)progress.color)/(float)PhotonEnum.PhotonColor.INFRARED;
			uvs[uvi] = new Vector2(0,uvY);
			uvi++;
			uvs[uvi] = new Vector2(1,uvY);
			uvi++;
			uvY += 1f / (float)(PhotonEnum.PhotonColor.INFRARED);
			uvs[uvi] = new Vector2(0,uvY);
			uvi++;
			uvs[uvi] = new Vector2(1,uvY);
			uvi++;
		}

		//Fill in the final row
		//Triangles!
		triangles [ti] = vi;
		triangles [ti + 2] = triangles [ti + 3] = vi + 1;
		triangles [ti + 1] = triangles [ti + 4] = vi + 2;
		triangles [ti + 5] = vi + 3;

		//Vertices!
		vertY = yAnchor + progressAccumulation * rect.rect.height;
		vertices [vi] = new Vector3(xAnchor, vertY);
		vi++;
		vertices [vi] = new Vector3 (xAnchor + xDelta, vertY);
		vi++;
		vertY = yAnchor + rect.rect.height;
		vertices [vi] = new Vector3(xAnchor, vertY);
		vi++;
		vertices [vi] = new Vector3 (xAnchor + xDelta, vertY);
		vi++;

		//UVS!
		uvY = ((float)PhotonEnum.PhotonColor.INFRARED - (float)progressList[progressList.Count-1].color)/(float)PhotonEnum.PhotonColor.INFRARED;
		uvs[uvi] = new Vector2(0,uvY);
		uvi++;
		uvs[uvi] = new Vector2(1,uvY);
		uvi++;
		uvY += 1f / (float)(PhotonEnum.PhotonColor.INFRARED);
		uvs[uvi] = new Vector2(0,uvY);
		uvi++;
		uvs[uvi] = new Vector2(1,uvY);
		uvi++;

		//uv.y == 0
//		vertices [0] = new Vector3 (xAnchor, vertY);
//		vertices [1] = new Vector3 (xAnchor + (xDelta * sizeX), vertY);
//		vertY += yAnchor;

//		for(int i = 2, y = 0; y <= sizeY; y++){// i = 2 to start after the initial padding
			//y corresponds to the current progress item
			//i is the current vertex index
//			Progress progress = progressList[y];
//			for(int x = 0; x <= sizeX; x++, i++){
				
//				float vertX = xAnchor + (xDelta * x);
//				vertices[i] = new Vector3(vertX,vertY);
//				uvs[i] = new Vector2((float)x / sizeX, (float)y / sizeY);
//			}

//			vertY += yDelta;
//		}

//		triangles = new int[sizeX * sizeY * 6];
//		for(int ti = 0, vi = 0, y = 0; y < sizeY; y++, vi++) {
//			for(int x = 0; x < sizeX; x++, ti += 6, vi++){
//				triangles[ti] = vi;
//				triangles[ti + 3] = triangles[ti + 2] = vi + 1;
//				triangles[ti + 4] = triangles[ti + 1] = vi + sizeX + 1;
//				triangles[ti + 5] = vi + sizeX + 2;
//			}
//		}
	}
}