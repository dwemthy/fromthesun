using System;
using UnityEngine;

public class DataCenter {
	public const int STATE_UNSTARTED 	= 0;
	public const int STATE_STARTED 		= 1;
	public const int STATE_PAUSED 		= 2;
	public const int STATE_LOST 		= 3;
	public const int STATE_WON			= 4;

    public const string EVENT_COLOR_CHANGE = "PlayerColorChange";
    public const string EVENT_START = "Start";
    public const string EVENT_WIN = "Win";
    public const string EVENT_LOSE = "Lose";
    public const string EVENT_QUIT = "Quit";
    public const string EVENT_PAUSE = "Pause";
    public const string EVENT_RESUME = "Resume";
    public const string EVENT_RESTART = "Restart";

    public const string PARAM_LEVEL = "Level";
    public const string PARAM_COLOR_CHANGE_TO = "ChangeTo";
    public const string PARAM_COLOR_CHANGE_FROM = "ChangeFrom";
    public const string PARAM_COLOR_DURATION = "DurationAsFromColor";
	
	private int _gameState = STATE_UNSTARTED;
	
	public int GameState {
		get { return _gameState; }
		set { _gameState = value; }
	}
	
	private DataCenter(){}
	
	private static DataCenter _instance;
	public static DataCenter Instance{
		get { if(_instance == null){
				_instance = new DataCenter();
			  }
			  
			  return _instance;
	  	}
  	}

	private Vector3 playerPosition;
	public Vector3 PlayerPosition {
		get{ return playerPosition; }
		set{ playerPosition = value; }
	}

    private long playerScore;
    public long Score {
        get { return playerScore; }
		set { playerScore = value; }
    }

    public void AddScore(long score) {
        playerScore += (long)Math.Floor(((float)score) * scoreMultiplier);
    }

	private PhotonEnum.PhotonColor playerColor = PhotonEnum.PhotonColor.INFRARED;

    public PhotonEnum.PhotonColor PlayerColor {
        get { return playerColor; }
        set { playerColor = value;
            UpdateScoreMultiplier();
        }
    }

	private int ammo = 0;
	public int Ammo {
		get { return ammo; }
		set { ammo = value; }
	}

    private float scoreMultiplier = 1;

    private void UpdateScoreMultiplier() {
        float colorDiff = ((int)PhotonEnum.PhotonColor.INFRARED + 1) - (int)playerColor;
        scoreMultiplier = 0.5f * colorDiff;
		scoreMultiplier = Mathf.Max (scoreMultiplier, 1);
    }

	public void UnlockLevel(string levelName){
		PlayerPrefs.SetInt (levelName, 1);
	}

	public void ClearSession(){
		ammo = 0;
		Score = 0;
		playerColor = PhotonEnum.PhotonColor.INFRARED;
	}
}