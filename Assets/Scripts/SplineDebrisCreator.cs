﻿using UnityEngine;
using System.Collections.Generic;

public class SplineDebrisCreator : BezierSpline {
    public enum SplineType {
        ROCK,
        CRYSTAL,
        MIXED,
        ROCK_HEAD_CRYSTAL_TAIL,
        CRYSTAL_HEAD_ROCK_TAIL,
        ROCK_BODY_CRYSTAL_BUTT,
        CRYSTAL_BODY_ROCK_BUTT,
		ROCK_ENDS_CRYSTAL_MIDDLE
    }
    
    private PhotonEnum.PhotonColor color;
    public PhotonEnum.PhotonColor Color{
    	get { return color; }
    	set { ColorizeDebris(value); }
	}

    public int frequency;
    public bool lookForward;
    public SplineType type;

	public float scaleMin = 0.7f;
	public float scaleMax = 1.5f;

	public int maxHealth;
	public Vector3 velocity;
    
	List<GameObject> debrisList = new List<GameObject>();

	public void SpawnSpline(Vector3 position, float xScale){
		if (gameObject.activeSelf) {
			transform.position = position;
			if(frequency <= 0) {
				return;
			}

			float stepSize = 1f / frequency;
			for(int f = 0; f <= frequency; f++) {
				GameObject item = InstantiateForTypeAtIndex(f);
				Vector3 spawnPos = GetPoint(f * stepSize);
				spawnPos.x *= xScale;
                spawnPos += position;

				item.transform.position = spawnPos;
				if (lookForward) {
					item.transform.LookAt(position + GetDirection(f * stepSize));
					item.transform.Rotate(0f, 180f, 0f);
				}
			}

			gameObject.SetActive (false);
		}
	}
			
	void ColorizeDebris(PhotonEnum.PhotonColor color){
		this.color = color;
		for(int i=0; i<debrisList.Count; i++){
			ColorHolder colorHolder = debrisList[i].GetComponent<ColorHolder>();
			if(colorHolder != null){
				colorHolder.SetPhotonColor(color);
			}
		}
	}

    private GameObject InstantiateForTypeAtIndex(int index) {
        GameObject spawned = null;
		Vector3 scaleBase = new Vector3 (0.5f, 0.5f, 0.5f);
        switch (type) {
			case SplineType.ROCK:
				spawned = ObjectPoolingManager.Instance.GetRock ();
				spawned.transform.localScale = scaleBase;
	            break;

            case SplineType.CRYSTAL:
                spawned = ObjectPoolingManager.Instance.GetCrystal();
                break;

            case SplineType.CRYSTAL_BODY_ROCK_BUTT:
                if (index < frequency - 1) {
                    spawned = ObjectPoolingManager.Instance.GetCrystal();
					spawned.transform.localScale = scaleBase;
                }else {
					spawned = ObjectPoolingManager.Instance.GetRock();
					spawned.transform.localScale =  scaleBase * 1.4f;
                }
                break;

            case SplineType.CRYSTAL_HEAD_ROCK_TAIL:
                if(index == 0) {
					spawned = ObjectPoolingManager.Instance.GetCrystal();
					spawned.transform.localScale = scaleBase * 1.5f;
                }else {
					spawned = ObjectPoolingManager.Instance.GetRock();
					spawned.transform.localScale = scaleBase;
                }
                break;

            case SplineType.ROCK_BODY_CRYSTAL_BUTT:
                if(index < frequency - 1) {
					spawned = ObjectPoolingManager.Instance.GetRock();
					spawned.transform.localScale = scaleBase;
                }else {
					spawned = ObjectPoolingManager.Instance.GetCrystal();
					spawned.transform.localScale = scaleBase;
                }
                break;

            case SplineType.ROCK_HEAD_CRYSTAL_TAIL:
                if (index == 0) {
					spawned = ObjectPoolingManager.Instance.GetRock();
					spawned.transform.localScale = scaleBase * 1.5f;
                } else {
					spawned = ObjectPoolingManager.Instance.GetCrystal();
					spawned.transform.localScale = scaleBase;
                }

                break;

            case SplineType.MIXED:
                if(Random.Range(0f, 1f) > 0.5f) {
					spawned = ObjectPoolingManager.Instance.GetRock();
					spawned.transform.localScale = scaleBase;
                }else {
					spawned = ObjectPoolingManager.Instance.GetCrystal();
					spawned.transform.localScale = scaleBase;
                }
                break;

			case SplineType.ROCK_ENDS_CRYSTAL_MIDDLE:
				int endSize = frequency / 3;
				if (index < endSize || index >= frequency - endSize) {
					spawned = ObjectPoolingManager.Instance.GetRock ();
					spawned.transform.localScale = scaleBase * Random.Range (0.8f, 1f);
				} else {
					spawned = ObjectPoolingManager.Instance.GetCrystal ();
					spawned.transform.localScale = scaleBase;
				}
				break;
        }

		ObjectMover mover = spawned.GetComponent<ObjectMover> ();
		if(mover == null){
			mover = spawned.GetComponentInChildren<ObjectMover> ();
		}

		if (mover != null) {
			mover.SetVelocity(velocity);
			if(mover.velocity.Equals(new Vector3())){
				mover.SetVelocity(new Vector3 (0, 0, -4));
			}
		}

		Destructable destructable = spawned.GetComponent<Destructable> ();
		if (destructable != null) {
			destructable.SetMaxHealth (Mathf.Min(1, maxHealth));
		} else {
			ClusterHolder cluster = spawned.GetComponent<ClusterHolder> ();
			if (cluster != null) {
				cluster.SetMaxHealth (Mathf.Min(1, maxHealth));
			}
		}
			
        if(spawned != null){
			ColorHolder colorHolder = spawned.GetComponent<ColorHolder>();
			if(colorHolder != null){
				colorHolder.SetPhotonColor(color);
			}

			debrisList.Add(spawned);
		}

        return spawned;
    }

	public override void SetFromSpawnData (SplineSpawnData data) {
		base.SetFromSpawnData (data);
		color = data.color;
		frequency = data.frequency;
		lookForward = data.lookingForward;
		scaleMin = data.scaleMin;
		scaleMax = data.scaleMax;
		maxHealth = data.maxHealth;
		velocity = data.velocity;
		Debug.Log("Set velocity in spline to " + velocity);
	}

	public override SplineSpawnData GetAsSpawnData () {
		SplineSpawnData data = base.GetAsSpawnData ();
		data.color = color;
		data.frequency = frequency;
		data.lookingForward = lookForward;
		data.scaleMax = scaleMax;
		data.scaleMin = scaleMin;
		data.maxHealth = maxHealth;
		data.velocity = velocity;

		return data;
	}
}