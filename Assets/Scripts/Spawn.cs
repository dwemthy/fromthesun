using System;
using UnityEngine;
using SpawnEnum;

[Serializable]
public class Spawn{
	public Vector3 position;
	public Vector3 velocity;
	public SpawnType type;
	public int maxHealth = 1;
    public GameObject prefab;
	public float scale = 1;
    public PhotonEnum.PhotonColor color = PhotonEnum.PhotonColor.BLUE;
	public SplineSpawnData splineData;

	public Spawn Clone(){
		Spawn clone = new Spawn ();

		clone.position = position;
		clone.velocity = velocity;
		clone.type = type;
		clone.maxHealth = maxHealth;
		clone.prefab = prefab;
		clone.scale = scale;
		clone.color = color;
		clone.splineData = splineData.clone();

		return clone;
	}
}