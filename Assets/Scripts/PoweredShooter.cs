﻿using UnityEngine;
using System.Collections;

public class PoweredShooter : PoweredModule {
	private int charge;

	public int chargeThreshold = 1;//How much charge to build up before shooting
	public int maxPower = 1;//Maximum power to put into a shot

	public GameObject shotPrefab;

	public override void AddPower (int power){
		charge += power;
		
		if (charge >= chargeThreshold) {
			int output = Mathf.Max (charge, maxPower);
			GameObject shot = Instantiate (shotPrefab);
			
			shot.transform.position = transform.position;

			EnemyShot eShot = shot.GetComponent<EnemyShot> ();
			if (eShot != null) {
				eShot.Power = output;
			}

            ObjectMover oMover = shot.GetComponent<ObjectMover>();
            if(oMover != null)
            {
                shootVelocity(oMover);
			}

			charge -= output;
			if (charge < 0) {
				charge = 0;
			}
		}
	}

    protected virtual void shootVelocity(ObjectMover oMover){
		oMover.SetVelocity(transform.root.GetComponent<ObjectMover>().velocity.normalized * oMover.velocity.magnitude);
    }
}
