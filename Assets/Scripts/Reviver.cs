﻿using UnityEngine;
using System.Collections;

public class Reviver : MonoBehaviour {
	public float reviveDelay = 1f;
	public Destructable reviveTarget;

	bool revivePending = false;
	float deadTime = 0;

	void FixedUpdate(){
		if (reviveTarget != null && DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			Collider collider = GetComponent<Collider> ();
			if (collider != null && !collider.enabled) {
				return;//TODO remove this after destructable spawns
			}

			if (revivePending) {
				deadTime += Time.deltaTime;
				if (deadTime >= reviveDelay) {
					reviveTarget.Revive ();
					revivePending = false;
				}
			} else if(reviveTarget.gameObject.activeSelf == false){
				revivePending = true;
				deadTime = 0;
			}
		}
	}
}
