﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PhotonEnum;
using UnityEngine.Analytics;

public class LevelManager : MonoBehaviour {
	public GameObject initialObjective;
	public float initialObjectiveDuration;
	public GameObject finalObjective;
	public float finalObjectiveDuration;
	public ProgressFiller progressFiller;
	public float levelDurationInSeconds;
	public AudioClip planetMusic;
	public AudioClip winMusic;
	public AudioClip loseMusic;
	public string nextLevel = "";

	private static bool showFinal;
	private static bool showingInitial;
	private static bool onPlanet;

	private float elapsedPlayTime;

	private float timeSinceInitialShown = 0;
	private float timeSinceFinalShown = 0;

	private static LevelManager instance;

	public static LevelManager Instance{
		get { return instance; }
	}

	public void Quit(){
		DataCenter.Instance.ClearSession ();
        IDictionary<string, object> eventParams = new Dictionary<string, object>();
        eventParams.Add(DataCenter.PARAM_LEVEL, Application.loadedLevel);
        Analytics.CustomEvent(DataCenter.EVENT_QUIT, eventParams);
        Application.LoadLevel("MainMenu");
	}

	public void Restart(){
		DataCenter.Instance.ClearSession ();
        IDictionary<string, object> eventParams = new Dictionary<string, object>();
        eventParams.Add(DataCenter.PARAM_LEVEL, Application.loadedLevel);
        Analytics.CustomEvent(DataCenter.EVENT_RESTART, eventParams);
        ObjectPoolingManager.Instance.Destroy();
		Application.LoadLevel(Application.loadedLevel);
		Time.timeScale = 1;
	}

	public void GoToMenu() {
		ObjectPoolingManager.Instance.Destroy();
		Application.LoadLevel("MainMenu");
	}

	public void GoToNextLevel(){
		Application.LoadLevel(nextLevel);
	}

	public void Win(){
		AudioSource musicSource = Camera.main.GetComponent<AudioSource> ();
		if (musicSource != null && winMusic != null) {
			musicSource.clip = winMusic;
			musicSource.loop = true;
			musicSource.Play ();
		}
		DataCenter.Instance.GameState = DataCenter.STATE_WON;
		if (!nextLevel.Equals ("")) {
			DataCenter.Instance.UnlockLevel (nextLevel);
			UIManager.Instance.showEnd (true, true);
		} else {
			//Don't show the end screen for earth
			//UIManager.Instance.showEnd (true, false);
        }
        IDictionary<string, object> eventParams = new Dictionary<string, object>();
        eventParams.Add(DataCenter.PARAM_LEVEL, Application.loadedLevel);
        Analytics.CustomEvent(DataCenter.EVENT_WIN, eventParams);
        Time.timeScale = 0f;
	}

	public void Lose(){
		AudioSource musicSource = Camera.main.GetComponent<AudioSource> ();
		if (musicSource != null && loseMusic != null) {
			musicSource.clip = loseMusic;
			musicSource.loop = true;
			musicSource.Play ();
		}
		DataCenter.Instance.GameState = DataCenter.STATE_LOST;
		UIManager.Instance.showEnd (false, false);
        IDictionary<string, object> eventParams = new Dictionary<string, object>();
        eventParams.Add(DataCenter.PARAM_LEVEL, Application.loadedLevel);
        Analytics.CustomEvent(DataCenter.EVENT_LOSE, eventParams);
        Time.timeScale = 0;
	}

	public void AddScore(int toAdd, Vector3 position){
		DataCenter.Instance.AddScore(toAdd);

		if(position != new Vector3()){
			GameObject scoreAward = ObjectPoolingManager.Instance.GetScoreText();
			scoreAward.transform.position = position;
			TextMesh scoreAwardTextMesh = scoreAward.GetComponent<TextMesh>();
			scoreAwardTextMesh.color = DataCenter.Instance.PlayerColor.GetColor();
			scoreAwardTextMesh.text = "" + toAdd;
		}
	}

	void Start(){
		instance = this;
		if (initialObjective != null) {
			initialObjective.SetActive (true);
			showingInitial = true;
		}
		onPlanet = false;
		timeSinceInitialShown = 0f;
		elapsedPlayTime = 0f;
        IDictionary<string, object> eventParams = new Dictionary<string, object>();
        eventParams.Add(DataCenter.PARAM_LEVEL, Application.loadedLevel);
        Analytics.CustomEvent(DataCenter.EVENT_START, eventParams);
    }


	void Update(){
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			if (initialObjective != null && initialObjective.activeSelf) {
				timeSinceInitialShown += Time.deltaTime;
				if (timeSinceInitialShown >= initialObjectiveDuration) {
					showingInitial = false;
					initialObjective.SetActive (false);
				}
			}

			if (finalObjective != null && finalObjective.activeSelf) {
				timeSinceFinalShown += Time.deltaTime;
				if (timeSinceFinalShown >= finalObjectiveDuration) {
					finalObjective.SetActive (false);
				}
			}

			if (showFinal) {
				if (finalObjective != null) {
					finalObjective.SetActive (true);
				}
				showFinal = false;

				AudioSource musicSource = Camera.main.GetComponent<AudioSource> ();
				if (musicSource != null && planetMusic != null) {
					musicSource.clip = planetMusic;
					musicSource.loop = true;
					musicSource.Play ();
				}
			}

			if (!onPlanet) {
				if (progressFiller != null) {
					progressFiller.AddFill (ScaledTime () / levelDurationInSeconds);
				}
				elapsedPlayTime += ScaledTime();
			}
		}
	}

	public static bool ShowingInitial(){
		return showingInitial;
	}

	public static void ShowFinal(){
		showFinal = true;
		onPlanet = true;
	}

	public static float ScaledTime(){
		return Time.deltaTime * (1f / PhotonUtils.scaleForPlayerColor ());
	}
}