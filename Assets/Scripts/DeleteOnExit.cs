﻿	using UnityEngine;
using System.Collections;

public class DeleteOnExit : MonoBehaviour {
	public bool onlyDeactivate = false;
	void OnTriggerExit(Collider other){
		if(onlyDeactivate){
			other.gameObject.SetActive(false);
		}else{
			Destroy (other.gameObject);
		}
	}
}