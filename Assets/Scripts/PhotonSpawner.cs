﻿using UnityEngine;
using System.Collections;

public class PhotonSpawner : MonoBehaviour {
	public float spawnInterval;
	
	float timeSinceSpawn = 0;
	
	public GameObject photonPrefab;
	public GameObject shotPrefab;

	// Use this for initialization
	void Start () {
		ObjectPoolingManager.Instance.CreatePhotonPool(photonPrefab);
		ObjectPoolingManager.Instance.CreateShotPool(shotPrefab);
		SpawnPhotonOnTopAtRandomX ();
	}
	
	// Update is called once per frame
	void Update () {
		if(DataCenter.Instance.GameState == DataCenter.STATE_STARTED){
			timeSinceSpawn += LevelManager.ScaledTime();
			if (timeSinceSpawn >= spawnInterval) {
				SpawnPhotonOnTopAtRandomX();
				timeSinceSpawn = 0;
			}
		}
	}

	void SpawnPhotonOnTopAtRandomX(){
		GameObject photon = ObjectPoolingManager.Instance.GetPhoton();
		if(photon == null){
			return;
		}
		Vector3 photonPos = transform.position;
		photonPos.x = Random.Range (transform.position.x - transform.localScale.x/2,
		                            transform.position.x + transform.localScale.x/2);
		photonPos.z = transform.position.z + transform.localScale.z / 2;

		GoldenAppleOfTheSun apple = photon.GetComponent<GoldenAppleOfTheSun>();
		if (apple != null) {
			int color = (int)apple.RandomizeColor();
			int playerColor = (int)DataCenter.Instance.PlayerColor;
//			if(color < playerColor){
				photonPos.z = transform.position.z - transform.localScale.z /2;
//			}else if(color > playerColor){
//				photonPos.z = transform.position.z + transform.localScale.z /2;
//			}else if(Random.Range(0, 2) == 0){
//				photonPos.z = transform.position.z - transform.localScale.z /2;
//			}else{
//				photonPos.z = transform.position.z + transform.localScale.z /2;
//			}
		}

		photon.transform.position = photonPos;
	}
}
