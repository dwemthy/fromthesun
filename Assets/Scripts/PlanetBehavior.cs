﻿using UnityEngine;
using System.Collections.Generic;

public class PlanetBehavior : ClusterHolder {
	protected Vector3 destination;
	public Vector3 Destination {
		set { destination = value; }
	}
	private float destinationTolerence = 0.001f;

	bool flashOne = false;
	bool flashTwo = false;
	bool flashThree = false;
	bool flashFour = false;
	bool flashFive = false;
	bool flashSix = false;

	public GameObject debrisPrefab;
	public int debrisCount = 4;
	public float debrisSpeed = 4;

	private bool dying;
	private float timeDying = 0;

	// Use this for initialization
	public virtual void Start () {
		Transform[] allChildren = GetComponentsInChildren<Transform>();
		int planetaryChildren = 0;
		foreach(Transform child in allChildren) {
			if (child.tag.Equals("Planet")) {
				planetaryChildren++;
			}
		}

		children = new Transform[planetaryChildren];
		planetaryChildren = 0;
		foreach (Transform child in allChildren) {
			if (child.tag.Equals("Planet")) {
				children[planetaryChildren] = child;
				planetaryChildren++;
			}
		}
	}
	
	public virtual void FixedUpdate () {
		base.FixedUpdate();
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			Vector3 toDestination = destination - transform.position;
			if (toDestination.magnitude > destinationTolerence) {
				toDestination = toDestination.normalized * Time.deltaTime;
				transform.position += toDestination;
			}

			if (dying) {
				timeDying+= Time.deltaTime;
				if(timeDying > 0.05){
					if(timeDying < 0.1){
						if(!flashOne){
							flashOne = true;
							Camera.main.GetComponent<NegativeFlash>().flash(0.15f);
						}
					}else{
						if(timeDying < 0.3){
							if(!flashTwo){
								flashTwo = true;
								Camera.main.GetComponent<NegativeFlash>().flash(0.1f);
							}
						}else {
							if(timeDying < 0.45f){
								if(!flashThree){
									flashThree = true;
									Camera.main.GetComponent<NegativeFlash>().flash(0.1f);
									Time.timeScale = 0.6f;
								}
							}else{
								if(timeDying < 0.65f){
									if(!flashFour){
										flashFour = true;
										Camera.main.GetComponent<NegativeFlash>().flash(0.07f);
									}
								}else{
									if(timeDying < 0.8f){
										if(!flashFive){
											flashFive = true;
											Camera.main.GetComponent<NegativeFlash>().flash(0.05f);
											Time.timeScale = 0.4f;
										}
									}else{
										if(timeDying < 0.9f){
											if(!flashSix){
												flashSix = true;
												Camera.main.GetComponent<NegativeFlash>().flash(0.03f);
											}
										}else{
											Time.timeScale = 1f;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public virtual void Damage(int damage, Vector3 location){
		CameraShake shake = Camera.main.gameObject.GetComponent<CameraShake> ();
		shake.shakeDuration = 0.1f;

		if(debrisPrefab != null){
			for (int i = 0; i < debrisCount; i++) {
				GameObject debrisObj = Instantiate (debrisPrefab);
				float spawnAngle = Random.Range(0, Mathf.PI);
				Vector3 spawnVelocity = new Vector3(Mathf.Cos(spawnAngle), 0, -Mathf.Sin(spawnAngle));

				ObjectMover mover = debrisObj.GetComponent<ObjectMover> ();
				if (mover != null) {
					mover.velocity = spawnVelocity.normalized * debrisSpeed;
				}

				debrisObj.transform.position = location;
			}
		}
	}
	
	public virtual void Die(){
		if (!dying) {
			dying = true;
			timeDying = 0;
			Camera.main.GetComponent<NegativeFlash> ().flash(0.03f);
			LevelManager.Instance.AddScore (GetScore (), transform.position);
			LevelManager.Instance.Win ();
			Time.timeScale = 0.7f;
		}
	}

    public virtual int GetScore() {
		return children.Length * 100;
    }
}