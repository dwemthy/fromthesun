﻿using UnityEngine;
using System.Collections;

public class StartScreenController : MonoBehaviour {
	public void StartGame(){
		DataCenter.Instance.GameState = DataCenter.STATE_STARTED;
		transform.gameObject.SetActive (false);
	}
}