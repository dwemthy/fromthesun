﻿using PhotonEnum;
using UnityEngine;

public class PhotonUtils {
	public const float SCALE_INFRARED = 1.2f;
	public const float SCALE_RED = 1.1f;
	public const float SCALE_ORANGE = 1f;
	public const float SCALE_YELLOW = .9f;
	public const float SCALE_GREEN = .8f;
	public const float SCALE_BLUE = .7f;
	public const float SCALE_PURPLE = .6f;
	public const float SCALE_ULTRAVIOLET = .5f;

	public static float scaleForColor(PhotonColor color){
		float scale = SCALE_INFRARED;
		switch (color) {
			case PhotonColor.INFRARED:
				scale = SCALE_INFRARED;
				break;
			case PhotonColor.RED:
				scale = SCALE_RED;
				break;
			case PhotonColor.ORANGE:
				scale = SCALE_ORANGE;
				break;
			case PhotonColor.YELLOW:
				scale = SCALE_YELLOW;
				break;
			case PhotonColor.GREEN:
				scale = SCALE_GREEN;
				break;
			case PhotonColor.BLUE:
				scale = SCALE_BLUE;
				break;
			case PhotonColor.PURPLE:
				scale = SCALE_PURPLE;
				break;
			case PhotonColor.ULTRAVIOLET:
				scale = SCALE_ULTRAVIOLET;
				break;
		}

		return scale;
	}

	public static Vector3 vectorScaleForColor(PhotonColor color){
		float scale = scaleForColor (color);
		return new Vector3 (scale, scale, scale);
	}

	public static float scaleForPlayerColor(){
		return scaleForColor (DataCenter.Instance.PlayerColor);
	}
}