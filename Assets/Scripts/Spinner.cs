﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {
	public float rotationSpeed = 90;

	void FixedUpdate () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			float scale = Time.deltaTime * PhotonUtils.scaleForPlayerColor ();

			Vector3 rotationDelta = new Vector3 (0, rotationSpeed * scale, 0);
			transform.Rotate (rotationDelta);
		}
	}
}
