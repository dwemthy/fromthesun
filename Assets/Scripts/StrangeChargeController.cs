﻿using UnityEngine;
using System.Collections;
using PhotonEnum;

public class StrangeChargeController : MonoBehaviour {
	public float lifeDurationTotal = 3f;
	private float lifeDuration = 0f;
	public float speed = 1f;
	
	// Update is called once per frame
	void Update () {
		PhotonColor colorToSet = (PhotonColor)Random.Range(1, 9);
		Color color = colorToSet.GetColor();
	
		GetComponent<ParticleSystem>().startColor = color;
		MeshRenderer renderer = GetComponent<MeshRenderer> ();
		if (renderer != null) {		
			renderer.material.color = color;
		}

		lifeDuration += Time.deltaTime;
		if(lifeDuration >= lifeDurationTotal){
			transform.root.gameObject.SetActive(false);
			lifeDuration = 0;
		}

		transform.Translate(new Vector3(0, 0, -speed*Time.deltaTime));
	}
}