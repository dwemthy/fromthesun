﻿using UnityEngine;
using System.Collections;

public class ObjectMover : MonoBehaviour {
	private float xRotationSpeed, yRotationSpeed, zRotationSpeed;

	public bool rotateX, rotateY, rotateZ;
	public float minRotationSpeed = 0;
	public float maxRotationSpeed;
	public Vector3 velocity;

	private Rigidbody rigiBod;
	private float baseMagnitude;

	// Use this for initialization
	void Start () {
		xRotationSpeed = rotateX ? Random.Range (minRotationSpeed, maxRotationSpeed) : 0;
		yRotationSpeed = rotateY ? Random.Range (minRotationSpeed, maxRotationSpeed) : 0;
		zRotationSpeed = rotateZ ? Random.Range (minRotationSpeed, maxRotationSpeed) : 0;

		rigiBod = GetComponent<Rigidbody>();
		if(rigiBod == null){
			rigiBod = GetComponentInChildren<Rigidbody>();
		}

		baseMagnitude = velocity.magnitude;
	}

	public void SetVelocity(Vector3 velocity){
		this.velocity = velocity;
		baseMagnitude = velocity.magnitude;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			float scale = Time.deltaTime * PhotonUtils.scaleForPlayerColor ();

			Vector3 rotationDelta = new Vector3 (xRotationSpeed * scale, yRotationSpeed * scale, zRotationSpeed * scale);
			transform.Rotate (rotationDelta);

			RecalculateSpeed ();
			transform.position += velocity*Time.deltaTime;
		}
	}

	private void RecalculateSpeed(){
		float scale = 1f / PhotonUtils.scaleForPlayerColor();
		velocity = velocity.normalized * (baseMagnitude * scale);
	}
}