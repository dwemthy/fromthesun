﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Collections;

namespace PhotonEnum{
	public static class Extensions{
		public static Color GetColor(this PhotonColor photonColor){
			Color color = Color.gray;
			
			switch(photonColor){
			case PhotonColor.INFRARED:
				color = new Color(0.5f, 0.08f, 0.13f);
				break;
			case PhotonColor.RED:
				color = new Color(1f, 0.04f, 0.0f);
				break;
			case PhotonColor.ORANGE:
				color = new Color(1f, 0.57f, 0f);
				break;
			case PhotonColor.YELLOW:
				color = new Color(1f, 1f, 0f);
				break;
			case PhotonColor.GREEN:
				color = new Color(0.0f, 0.79f, 0.21f);
				break;
			case PhotonColor.BLUE:
				color = new Color(0.05f, 0.23f, 0.97f);
				break;
			case PhotonColor.PURPLE:
				color = new Color(0.69f, 0.2f, 0.7f);
				break;
			case PhotonColor.ULTRAVIOLET:
				color = new Color(1f, 0.87f, 1f);
				break;
			}
			
			return color;
        }

        public static string ColorName(this PhotonColor photonColor)
        {
            string name = "color";

            switch (photonColor)
            {
                case PhotonColor.INFRARED:
                    name = "Infrared";
                    break;
                case PhotonColor.RED:
                    name = "Red";
                    break;
                case PhotonColor.ORANGE:
                    name = "Orange";
                    break;
                case PhotonColor.YELLOW:
                    name = "Yellow";
                    break;
                case PhotonColor.GREEN:
                    name = "Green";
                    break;
                case PhotonColor.BLUE:
                    name = "Blue";
                    break;
                case PhotonColor.PURPLE:
                    name = "Purple";
                    break;
                case PhotonColor.ULTRAVIOLET:
                    name = "Ultraviolet";
                    break;
            }

            return name;
        }
    }
	
	public enum PhotonColor{
		INFRARED=8,
		RED=7,
		ORANGE=6,
		YELLOW=5,
		GREEN=4,
		BLUE=3,
		PURPLE=2,
		ULTRAVIOLET=1
	}
}