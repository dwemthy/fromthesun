using  UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ObjectPoolingManager
{
	
	//the variable is declared to be volatile to ensure that
	//assignment to the instance variable completes before the
	//instance variable can be accessed.
	private static volatile ObjectPoolingManager instance;
	
	private static ObjectPool rockPool, crystalPool, radPool, dustPool, splinePool,
	 photonPool, shotPool, photonDestructionPool, scoreTextPool, rockTetraPool;
	
	//object for locking
	private static object syncRoot = new System.Object();
	
	/// <summary>
	/// Constructor for the class.
	/// </summary>
	private ObjectPoolingManager()
	{
	}
	
	/// <summary>
	/// Property for retreiving the singleton.  See msdn documentation.
	/// </summary>
	public static ObjectPoolingManager Instance
	{
		get
		{
			//check to see if it doesnt exist
			if (instance == null)
			{
				//lock access, if it is already locked, wait.
				lock (syncRoot)
				{
					//the instance could have been made between
					//checking and waiting for a lock to release.
					if (instance == null)
					{
						//create a new instance
						instance = new ObjectPoolingManager();
					}
				}
			}
			//return either the new instance or the already built one.
			return instance;
		}
	}
	
	public void CreateRockPool(GameObject rock){
		if(rockPool != null){
			rockPool.Destroy();
		}
		rockPool = new ObjectPool(rock, 5, 30);
	}
	
	public void CreateRadPool(GameObject rad){
		if(radPool != null){
			radPool.Destroy();
		}
		radPool = new ObjectPool(rad, 5, 30);
	}
	
	public void CreateDustPool(GameObject dust){
		if(dustPool != null){
			dustPool.Destroy();
		}
		dustPool = new ObjectPool(dust, 5, 30);
	}
	
	public void CreateCrystalPool(GameObject crystal){
		if(crystalPool != null){
			crystalPool.Destroy();
		}
		crystalPool = new ObjectPool(crystal, 5, 30);
	}

	public void CreateSplinePool(GameObject spline){
		if (splinePool != null) {
			splinePool.Destroy ();
		}

		splinePool = new ObjectPool (spline, 2, 10);
	}
	
	public void CreatePhotonPool(GameObject photon){
		if(photonPool != null){
			photonPool.Destroy();
		}
		photonPool = new ObjectPool(photon, 5, 30);
	}
	
	public void CreateShotPool(GameObject shot){
		if(shotPool != null){
			shotPool.Destroy();
		}
		shotPool = new ObjectPool(shot, 5, 30);
	}
	
	public void CreatePhotonDestructionPool(GameObject photonDestruction){
		if(photonDestructionPool != null){
			photonDestructionPool.Destroy();
		}
		
		photonDestructionPool = new ObjectPool(photonDestruction, 5, 30);
	}
	
	public void CreateScoreTextPool(GameObject scoreText){
		if(scoreTextPool != null){
			scoreTextPool.Destroy();
		}
		
		scoreTextPool = new ObjectPool(scoreText, 5, 10);
	}

	public void CreateRockTetraPool(GameObject rockTetra){
		if (rockTetraPool != null) {
			rockTetraPool.Destroy ();
		}

		rockTetraPool = new ObjectPool (rockTetra, 10, 50);
	}
	
	public GameObject GetRock(){
		GameObject rock = null;
		if(rockPool != null){
			rock = rockPool.GetObject();
		}
		return rock;
	}
	
	public GameObject GetCrystal(){
		GameObject crystal = null;
		if(crystalPool != null){
			crystal = crystalPool.GetObject();
		}
		return crystal;
	}
	
	public GameObject GetRad(){
		GameObject rad = null;
		if(radPool != null){
			rad = radPool.GetObject();
		}
		return rad;
	}
	
	public GameObject GetDust(){
		GameObject dust = null;
		if(dustPool != null){
			dust = dustPool.GetObject();
		}
		return dust;
	}
	
	public GameObject GetPhoton(){
		GameObject photon = null;
		if(photonPool != null){
			photon = photonPool.GetObject();
		}
		return photon;
	}

	public GameObject GetSpline(){
		GameObject spline = null;
		if (splinePool != null) {
			spline = splinePool.GetObject ();
		}

		return spline;
	}
	
	public GameObject GetShot(){
		GameObject shot = null;
		if(shotPool != null){
			shot = shotPool.GetObject();
		}
		return shot;
	}
	
	public GameObject GetPhotonDestruction(){
		GameObject photonDestruction  = null;
		if(photonDestructionPool != null){
			photonDestruction = photonDestructionPool.GetObject();
		}
		return photonDestruction;
	}
	
	public GameObject GetScoreText(){
		GameObject scoreText = null;
		if(scoreTextPool != null){
			scoreText = scoreTextPool.GetObject();
		}
		return scoreText;
	}

	public GameObject GetRockTetra(){
		GameObject rockTetra = null;
		if(rockTetraPool != null){
			rockTetra = rockTetraPool.GetObject ();
		}
		return rockTetra;
	}
	
	public void Destroy() {
		rockPool.Destroy();
		rockPool = null;
		crystalPool.Destroy();
		crystalPool = null;
		radPool.Destroy();
		radPool = null;
		dustPool.Destroy();
		dustPool = null;
		splinePool.Destroy ();
		splinePool = null;
		photonPool.Destroy();
		photonPool = null;
		shotPool.Destroy();
		shotPool = null;
		photonDestructionPool.Destroy();
		photonDestructionPool = null;
		scoreTextPool.Destroy();
		scoreTextPool = null;
		rockTetraPool.Destroy ();
		rockTetraPool = null;
	}
}