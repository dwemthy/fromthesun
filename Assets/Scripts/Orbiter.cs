﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbiter : MonoBehaviour {
	public Transform orbitTarget;
	public float orbitDistance;
	public float orbitSpeed;

	public float currentAngle;

    private bool launching = true;
    private float launchDistance = 0f;
	
	// Update is called once per frame
	void Update () {
        if(orbitTarget != null)
        {
            if (launching)
            {
                float x = orbitTarget.position.x + Mathf.Cos(currentAngle) * launchDistance;
                float z = orbitTarget.position.z + Mathf.Sin(currentAngle) * launchDistance;

                Vector3 pos = transform.position;
                pos.x = x;
                pos.z = z;
                transform.position = pos;

                launchDistance += Time.deltaTime * orbitSpeed * 3;
                if (launchDistance >= orbitDistance)
                {
                    launching = false;
                }
            }
            else
            {
                float x = orbitTarget.position.x + Mathf.Cos(currentAngle) * orbitDistance;
                float z = orbitTarget.position.z + Mathf.Sin(currentAngle) * orbitDistance;

                Vector3 pos = transform.position;
                pos.x = x;
                pos.z = z;
                transform.position = pos;

                if (orbitTarget.gameObject.activeSelf)
                {
                    currentAngle += orbitSpeed * Time.deltaTime;
                }
                else
                {
                    orbitDistance += orbitSpeed * Time.deltaTime;
                }
            }
        }
	}

    public void MarkUnlaunched()
    {
        launching = true;
    }

    public void MarkLaunched()
    {
        launching = false;
    }
}
