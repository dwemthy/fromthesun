﻿using UnityEngine;
using System.Collections;

public class AudioShifter : MonoBehaviour {
	public float easing = 1f;

	float startingPitch;
	AudioSource source;

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
		if (source != null) {
			startingPitch = source.pitch;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (source != null) {
			//Apply easing to the amount by which the scale is different from 1
			float scaleBase = 1f - PhotonUtils.scaleForPlayerColor ();
			scaleBase *= easing;
			//Add 1 to the base when applying the scale, so that the final scale used accounts for easing from 1
			source.pitch = startingPitch * (1f + scaleBase);
		}
	}
}
