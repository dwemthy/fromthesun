﻿using UnityEngine;
using System.Collections.Generic;
using PhotonEnum;

public class ShotController : MonoBehaviour {
	public float speed = 6f;

	private Rigidbody rigiBod;
	private PhotonColor photonColor;
	private AudioSource shotSource;
	
	void Start () {
		rigiBod = GetComponent<Rigidbody> ();
		shotSource = GetComponent<AudioSource> ();
	}

	void Update () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			rigiBod.velocity = new Vector3 (0, 0, speed);
		} else {
			rigiBod.velocity = new Vector3 ();
		}
	}


	public void Activate(){
		if (shotSource != null) {
			shotSource.Play ();
		}
	}
	
	public void SetPhotonColor(int color){
		SetPhotonColor ((PhotonColor)color);
	}
	
	public void SetPhotonColor(PhotonColor color){
		photonColor = color;
		
		MeshRenderer renderer = GetComponent<MeshRenderer> ();
		if (renderer != null) {		
			renderer.material.color = GetColor();
		}

		GetComponent<ParticleSystem> ().startColor = GetColor ();
	}
	
	public Color GetColor(){
		return GetPhotonColor ().GetColor ();
	}
	
	public PhotonColor GetPhotonColor(){
		return photonColor;
	}

	public int power(){
		//INFRARED = 8, UV = 1. power for IR = 1 (8 - 7), for UV = 8 ( 8 - 0 )
		return ((int)PhotonColor.INFRARED) - (((int)GetPhotonColor ()) - 1);
	}

	public int Damage(int damage){
		int dealt = damage;

		if (damage < power ()) {
			SetPhotonColor (
				(PhotonColor)(
					(int)GetPhotonColor () + dealt
				));
		} else {
			dealt = power ();
			transform.root.gameObject.SetActive(false);
		}

		return dealt;
	}

	void OnTriggerEnter(Collider other){
		if (transform.gameObject.activeSelf) {
			string otherTag = other.tag;
			
			if (otherTag.Equals ("Photon")) {
				other.transform.root.gameObject.SetActive(false);
			}else if(otherTag.Equals("Debris")){
				ColorHolder colorHolder = other.GetComponent<ColorHolder> ();
				bool killed = true;
				if(colorHolder != null){
					killed = colorHolder.hitsColor (photonColor);
				}

				if(killed){
					Destructable destructable = other.GetComponent<Destructable> ();
					if (destructable != null) {
						int dealt = destructable.Damage (power());
						Damage (dealt);
					}
				}
			}else if(otherTag.Equals("Planet")){
				Destructable destruct = other.transform.gameObject.GetComponent<Destructable> ();
				if (destruct != null) {
					int dealt = destruct.Damage (power ());
					Damage (dealt);

					if(destruct.Health <= 0) {
						PlanetBehavior planet = other.gameObject.GetComponentInParent<PlanetBehavior>();
						if (planet != null) {
							planet.Damage(power(), other.transform.position);
						}
					}
				}
			}
		}
	}
}