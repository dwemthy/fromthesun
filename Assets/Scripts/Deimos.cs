﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deimos : MonoBehaviour {
	public Transform mars;
	public float minOrbitDistance;
	public float preferredOrbitDistance;
	public float orbitSpeed;
	public float attractDistance;

	private float currentOrbitDistance;
	private float currentAngle = 0f;
	private Vector3 velocity;
	private bool lockedOn = false;

	void Start() {
		if(mars != null) {
			currentAngle = Mathf.Atan2(mars.transform.position.y - transform.position.y,
				mars.transform.position.x - transform.position.x);
		}

		velocity = new Vector3();
		currentOrbitDistance = preferredOrbitDistance;
	}

	void FixedUpdate() {
		if(mars != null) {
			Vector3 toPlayer = DataCenter.Instance.PlayerPosition - transform.position;
			if (toPlayer.magnitude <= attractDistance) {
				lockedOn = true;
			}

			if(lockedOn) {
				velocity = toPlayer.normalized * orbitSpeed;
				transform.position += velocity * Time.deltaTime;
			} else {
				float x = mars.position.x + Mathf.Cos(currentAngle) * currentOrbitDistance;
				float z = mars.position.z + Mathf.Sin(currentAngle) * currentOrbitDistance;

				if(currentOrbitDistance > preferredOrbitDistance) {
					currentOrbitDistance -= orbitSpeed * Time.deltaTime;
				}else if(currentOrbitDistance < preferredOrbitDistance) {
					currentOrbitDistance += orbitSpeed * Time.deltaTime;
				}

				Vector3 pos = transform.position;
				pos.x = x;
				pos.z = z;
				transform.position = pos;

				currentAngle += (orbitSpeed / currentOrbitDistance) * Time.deltaTime;
			}
		}
	}
}
