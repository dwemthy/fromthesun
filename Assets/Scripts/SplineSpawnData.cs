﻿using System;
using UnityEngine;

[Serializable]
public class SplineSpawnData{
	public Vector3[] points = new Vector3[]{};
	public BezierSpline.BezierControlPointMode[] modes = new BezierSpline.BezierControlPointMode[]{};
	public bool loop;
	public bool lookingForward;
	public int frequency = 15;
	public SplineDebrisCreator.SplineType type;
	public float scaleMin = 0.7f;
	public float scaleMax = 1.5f;
	public PhotonEnum.PhotonColor color;
	public int selectedIndex = 0;
	public int maxHealth = 1;
	public Vector3 velocity = new Vector3(0,0,-4);


	public int CurveCount {
		get {
			return (points.Length - 1) / 3;
		}
	}

	public SplineSpawnData (){
		points = new Vector3[] {
			new Vector3(-1f, 0f, 1f),
			new Vector3(-0.5f, 0f, 2f),
			new Vector3(0.5f, 0f, 3f),
			new Vector3(1f, 0f, 4f)
		};

		modes = new BezierSpline.BezierControlPointMode[] {
			BezierSpline.BezierControlPointMode.Free,
			BezierSpline.BezierControlPointMode.Free
		};
	}

	public SplineSpawnData clone(){
		SplineSpawnData clone = new SplineSpawnData ();

		clone.points = new Vector3[points.Length];
		Array.Copy (points, clone.points, points.Length);
		clone.modes = new BezierSpline.BezierControlPointMode[modes.Length];
		Array.Copy (modes, clone.modes, modes.Length);
		clone.loop = loop;
		clone.lookingForward = lookingForward;
		clone.frequency = frequency;
		clone.type = type;
		clone.scaleMin = scaleMin;
		clone.scaleMax = scaleMax;
		clone.color = color;
		clone.selectedIndex = selectedIndex;
		clone.velocity = velocity;

		return clone;
	}

	public BezierSpline.BezierControlPointMode GetControlPointMode(int index) {
		return modes[(index + 1) / 3];
	}

	public void SetControlPointMode( int index, BezierSpline.BezierControlPointMode mode) {
		int modeIndex = (index + 1) / 3;
		modes[modeIndex] = mode;
		if (loop) {
			if (modeIndex == 0) {
				modes[modes.Length - 1] = mode;
			} else if (modeIndex == modes.Length - 1) {
				modes[0] = mode;
			}
		}
		EnforceMode(index);
	}


	public void AddCurve () {
		Vector3 point = points[points.Length - 1];
		Array.Resize(ref points, points.Length + 3);
		point.z += 1f;
		points[points.Length - 3] = point;
		point.z += 1f;
		points[points.Length - 2] = point;
		point.z += 1f;
		points[points.Length - 1] = point;

		Array.Resize(ref modes, modes.Length + 1);
		modes[modes.Length - 1] = modes[modes.Length - 2];
		EnforceMode(points.Length - 4);

		if (loop) {
			points[points.Length - 1] = points[0];
			modes[modes.Length - 1] = modes[0];
			EnforceMode(0);
		}
	}

	public void RemoveCurve() {
		if(points.Length > 4) {
			Array.Resize(ref points, points.Length - 3);
			Array.Resize(ref modes, modes.Length - 1);
			if (loop) {
				points[points.Length - 1] = points[0];
				modes[modes.Length - 1] = modes[0];
				EnforceMode(0);
			}
		}
	}

	private void EnforceMode(int index) {
		int modeIndex = (index + 1) / 3;
		BezierSpline.BezierControlPointMode mode = modes[modeIndex];
		if(mode == BezierSpline.BezierControlPointMode.Free || !loop && (modeIndex == 0 || modeIndex == modes.Length - 1)) {
			return;
		}

		int middleIndex = modeIndex * 3;
		int fixedIndex, enforcedIndex;
		if(index <= middleIndex) {
			fixedIndex = middleIndex - 1;
			if(fixedIndex < 0) {
				fixedIndex = points.Length - 2;
			}
			enforcedIndex = middleIndex + 1;
			if(enforcedIndex >= points.Length) {
				enforcedIndex = 1;
			}
		}else {
			fixedIndex = middleIndex + 1;
			if(fixedIndex >= points.Length) {
				fixedIndex = 1;
			}
			enforcedIndex = middleIndex - 1;
			if(enforcedIndex < 0) {
				enforcedIndex = points.Length - 2;
			}
		}

		Vector3 middle = points[middleIndex];
		Vector3 enforcedTangent = middle - points[fixedIndex];
		if(mode == BezierSpline.BezierControlPointMode.Aligned) {
			enforcedTangent = enforcedTangent.normalized * Vector3.Distance(middle, points[enforcedIndex]);
		}
		points[enforcedIndex] = middle + enforcedTangent;
	}

	public Vector3 GetPoint(Transform transform, float t) {
		int i;
		if(t >= 1f) {
			t = 1f;
			i = points.Length - 4;
		}else {
			t = Mathf.Clamp01(t) * CurveCount;
			i = (int)t;
			t -= i;
			i *= 3;
		}

		Vector3 bezP = Bezier.GetPoint (points [i], points [i + 1], points [i + 2], points [i + 3], t);

		return bezP;
	}
}