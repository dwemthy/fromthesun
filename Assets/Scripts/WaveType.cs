namespace WaveEnum{
	public enum WaveType{
		MANUAL,
		FIVE_TIMES_FIVE_ROCK,
		FIVE_CRYSTAL,
		PREFAB_RAIN,
		ROCK_HELIX,
		FIVE_OF_PREFAB,
		SPLINE
	}
}