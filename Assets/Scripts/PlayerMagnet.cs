﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ObjectMover))]
public class PlayerMagnet : MonoBehaviour {
    public bool facePlayer = true;
    public float magnetDistance = 3f;

    private ObjectMover mover;
    
	void Start () {
        mover = GetComponent<ObjectMover>();
	}

    void FixedUpdate() {
        Vector3 toPlayer = DataCenter.Instance.PlayerPosition - transform.position;
        if(toPlayer.magnitude <= magnetDistance) {
            float speed = mover.velocity.magnitude;
            mover.velocity = toPlayer.normalized * speed;

            if(facePlayer) {
                transform.forward = toPlayer;
            }
        }
    }
}
