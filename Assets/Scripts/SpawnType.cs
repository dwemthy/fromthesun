using System.Collections.Generic;

namespace SpawnEnum{
	public enum SpawnType{
		ROCK,
		CRYSTAL,
		DUST,
		RADIATION,
        PREFAB,
		SPLINE
	}
}