﻿using UnityEngine;
using System.Collections;

public class MoveToPosition : MonoBehaviour {
	public float duration;

	private Vector3 destination;
	private Vector3 difference;

	public void SetDestination(Vector3 dest){
		destination = dest;
		difference = destination - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 increment = (difference / duration) * LevelManager.ScaledTime();
		transform.position = transform.position + increment;
	}
}