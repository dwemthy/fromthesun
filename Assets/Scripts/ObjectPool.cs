using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ObjectPool{
	private List<GameObject> pooledObjects;
	private GameObject pooledObj;
	private Quaternion initialRotation;
	private int maxPoolSize;
	private Vector3 baseScale;
		
	public ObjectPool(GameObject obj, int initialPoolSize, int maxPoolSize){
		//instantiate a new list of game objects to store our pooled objects in.
		pooledObjects = new List<GameObject>();
		initialRotation = obj.transform.rotation;
		baseScale = obj.transform.localScale;
		
		//create and add an object based on initial size.
		for (int i = 0; i < initialPoolSize; i++){
			//instantiate and create a game object with useless attributes.
			//these should be reset anyways.
			GameObject nObj = GameObject.Instantiate(obj, Vector3.zero, Quaternion.identity) as GameObject;
			
			//make sure the object isn't active.
			nObj.SetActive(false);
			
			//add the object too our list.
			pooledObjects.Add(nObj);
		}
		
		//store our other variables that are useful.
		this.maxPoolSize = maxPoolSize;
		this.pooledObj = obj;
	}
	
	/// <summary>
	/// Returns an active object from the object pool without resetting any of its values.
	/// You will need to set its values and set it inactive again when you are done with it.
	/// </summary>
	/// <returns>Game Object of requested type if it is available, otherwise null.</returns>
	public GameObject GetObject(){
		//iterate through all pooled objects.
		for (int i = 0; i < pooledObjects.Count; i++){
			//look for the first one that is inactive.
			if (pooledObjects[i].activeSelf == false){
				//set the object to active.
				pooledObjects[i].SetActive(true);
				pooledObjects[i].transform.rotation = initialRotation;
				pooledObjects[i].transform.localScale = baseScale;
				//return the object we found.
				return pooledObjects[i];
			}
		}
		
		//Instantiate a new object.
		GameObject nObj = GameObject.Instantiate(pooledObj, Vector3.zero, Quaternion.identity) as GameObject;
		//set it to active since we are about to use it.
		nObj.SetActive(true);
		nObj.transform.rotation = initialRotation;
		nObj.transform.localScale = baseScale;
		
		//if we make it this far, we obviously didn't find an inactive object.
		//so we need to see if we can grow beyond our current count.
		if (this.maxPoolSize > this.pooledObjects.Count){
			//add it to the pool of objects
			pooledObjects.Add(nObj);
		}
		
		//return the object to the requestor.
		return nObj;
	}
	
	public void Destroy(){
		for(int i = 0; i < pooledObjects.Count; i++){
            if(pooledObjects[i] != null) {
                pooledObjects[i].SetActive(false);
            }
		}
        pooledObjects.Clear();
	}
}