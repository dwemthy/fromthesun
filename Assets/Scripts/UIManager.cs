﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class UIManager : MonoBehaviour{
	static string scoreFormat = "0000000000";

	private static UIManager instance;
	public static UIManager Instance {
		get { return instance; }
	}

	public GameObject pausePanel;
	public EndScreen endPanel;

	public Text scoreLabel;
	public Text ammoLabel;

	void Start(){
		instance = this;

		scoreLabel.text = DataCenter.Instance.Score.ToString(scoreFormat);
	}

	void FixedUpdate () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			scoreLabel.text = DataCenter.Instance.Score.ToString(scoreFormat);
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
				Pause();
			} else if (DataCenter.Instance.GameState == DataCenter.STATE_PAUSED) {
				Unpause();
			}
		}

		ammoLabel.text = DataCenter.Instance.Ammo.ToString ();
	}

	public void Pause(){
		DataCenter.Instance.GameState = DataCenter.STATE_PAUSED;
		pausePanel.SetActive(true);
        IDictionary<string, object> eventParams = new Dictionary<string, object>();
        eventParams.Add(DataCenter.PARAM_LEVEL, Application.loadedLevel);
        Analytics.CustomEvent(DataCenter.EVENT_PAUSE, eventParams);
    }

	public void Unpause(){
		DataCenter.Instance.GameState = DataCenter.STATE_STARTED;
		pausePanel.SetActive(false);
        IDictionary<string, object> eventParams = new Dictionary<string, object>();
        eventParams.Add(DataCenter.PARAM_LEVEL, Application.loadedLevel);
        Analytics.CustomEvent(DataCenter.EVENT_RESUME, eventParams);
    }

	public void showEnd(bool won, bool nextAvailable){
		endPanel.Show (won, nextAvailable);
	}

	public void hideEnd(){
		endPanel.gameObject.SetActive (false);
	}

}