﻿using UnityEngine;
using System.Collections;

public class NegativeFlash : MonoBehaviour {
	public Material material;

	float flashDuration = 0f;
	float timeSinceFlashStart = 0f;
	
	public void flash(float duration){
		timeSinceFlashStart = 0f;
		flashDuration = duration;
	}

	void OnRenderImage( RenderTexture src, RenderTexture dest ) {
		if (timeSinceFlashStart < flashDuration) {
			Graphics.Blit (src, dest, material);
		} else {
			Graphics.Blit (src, dest);
		}

		timeSinceFlashStart += Time.deltaTime;
	}
}