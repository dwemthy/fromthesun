﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFacer : MonoBehaviour {

	private TargetShooter shooter;

	void Start() {
		shooter = GetComponentInChildren<TargetShooter>();
	}
    
	void Update () {
        Vector3 playerPosition = DataCenter.Instance.PlayerPosition;
        Vector3 toPlayer = playerPosition - transform.position;
        toPlayer.Normalize();

		if(shooter != null) {
			shooter.SetToTarget(toPlayer);
		}

        Quaternion rotation = Quaternion.LookRotation(toPlayer, transform.up);
        transform.rotation = rotation;
	}
}
