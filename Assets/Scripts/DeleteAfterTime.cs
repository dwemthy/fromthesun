﻿using UnityEngine;
using System.Collections;

public class DeleteAfterTime : MonoBehaviour {
	public float lifeTime;
	public bool onlyDeactivate = false;
	
	float timeElapsed = 0f;
	
	// Update is called once per frame
	void Update () {
		timeElapsed += Time.deltaTime;
		if(timeElapsed > lifeTime){
			if(onlyDeactivate){
				transform.gameObject.SetActive(false);
				timeElapsed = 0f;
			}else{
				Destroy(transform.gameObject);
			}
		}
	}
}