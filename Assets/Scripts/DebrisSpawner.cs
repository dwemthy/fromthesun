﻿using UnityEngine;
using SpawnEnum;
using System.Collections.Generic;

public class DebrisSpawner : MonoBehaviour {
	public StaticFilter staticFilter;
	public GameObject debrisPrefab;
	public GameObject transparentPrefab;
	public GameObject dustPrefab;
	public GameObject radiationPrefab;
	public GameObject splinePrefab;
	public GameObject rockTetraPrefab;
	public GameObject scoreAwardPrefab;
	public GameObject deathEffectPrefab;

	public List<Wave> waves;
	public float delayBetweenLastWaveAndPlanet = 5;
	public GameObject planetPrefab = null;
    public RectTransform playerUI;
	public List<Wave> planetWaves;
	public Vector3 defaultVelocity = new Vector3 (0, 0, -4);
	
	float timeToStartPlanet = -1;
	bool planetStarted = false;
	int planetWaveIndex = 0;
	float timeSincePlanetWave = 0;

	private double totalTime = 0;
	
	private float width, height;

	private static float xMax, xMin;
	public static float XMax{
		get { return xMax; }
	}
	public static float XMin{
		get { return xMin; }
	}
	private float uiWidth;

	private bool rainingPrefabs = false;
	private GameObject rainPrefab;
	private float rainElapsed = 0;
	private float rainDuration = 0;
	private float rainFrequency = 0;
	private float timeSinceLastRainSpawn = 0;
	
	void Start(){
		//CREATE OBJECT POOLS
		ObjectPoolingManager.Instance.CreatePhotonDestructionPool(deathEffectPrefab);
		ObjectPoolingManager.Instance.CreateScoreTextPool(scoreAwardPrefab);
		ObjectPoolingManager.Instance.CreateRockPool(debrisPrefab);
		ObjectPoolingManager.Instance.CreateCrystalPool(transparentPrefab);
		ObjectPoolingManager.Instance.CreateRadPool(radiationPrefab);
		ObjectPoolingManager.Instance.CreateDustPool(dustPrefab);
		ObjectPoolingManager.Instance.CreateSplinePool (splinePrefab);
		ObjectPoolingManager.Instance.CreateRockTetraPool (rockTetraPrefab);
		
		//Size to fit camera
        uiWidth = playerUI.rect.width;
        uiWidth = Mathf.Abs(Camera.main.ScreenToWorldPoint(new Vector3(uiWidth, 0, 0)).x)*0.5f;

		float camHeight = 2f * Camera.main.orthographicSize;
		float camWidth = camHeight * Camera.main.aspect;
		Vector3 scale = transform.localScale;
		scale.x = camWidth - uiWidth;
		scale.z = camHeight;
		transform.localScale = scale;
		
		width = camWidth - uiWidth;
		height = camHeight;
        Vector3 pos = transform.position;
        pos.x -= uiWidth*0.5f;
		transform.position = pos;

		xMax = (width * 0.5f) + pos.x;
		xMin = xMax - width;
		
		PopulateWaves();
	}

	void FixedUpdate () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED && !LevelManager.ShowingInitial()) {
			totalTime += LevelManager.ScaledTime();
			
			if (waves.Count > 0 && waves [0].timeTrigger <= totalTime) {
				Wave wave = waves [0];
				waves.Remove (wave);
				if (waves.Count == 0) {
					timeToStartPlanet = (float)(totalTime + delayBetweenLastWaveAndPlanet);
				}

				if(wave.waveType == WaveEnum.WaveType.MANUAL){
					for (int i =0; i<wave.spawns.Count; i++) {
						SpawnDebris (wave.spawns [i]);
					}
				}

				if (wave.waveType == WaveEnum.WaveType.PREFAB_RAIN) {
					rainingPrefabs = true;
					rainDuration = wave.duration;
					rainFrequency = wave.frequency;
					rainElapsed = 0f;
					rainPrefab = wave.prefab;
				}else if(wave.waveType == WaveEnum.WaveType.SPLINE) {
					GameObject debris = ObjectPoolingManager.Instance.GetSpline ();
					SplineDebrisCreator spline = debris.GetComponent<SplineDebrisCreator>();
					if(spline != null){
						spline.SetFromSpawnData(wave.splineData);
						Vector3 point = new Vector3 (transform.position.x / (width*0.5f), 0, 0);
						spline.SpawnSpline (point, width * 0.5f);
					}
				}
			} else if(planetPrefab != null){
				if (timeToStartPlanet > 0 && totalTime > timeToStartPlanet && !planetStarted) {
					planetStarted = true;
					GameObject planet = Instantiate (planetPrefab);
					PlanetBehavior behavior = planet.GetComponentInChildren<PlanetBehavior>();
					if(behavior != null){
						Vector3 destination = transform.position;
						destination.z += height * 0.25f;
						behavior.Destination = destination;

						if(behavior is EarthController) {
							EarthController earth = behavior as EarthController;
							earth.staticFilter = staticFilter;
							earth.uiPanel = playerUI.gameObject;
						}
					}
					LevelManager.ShowFinal ();
					timeSincePlanetWave = 0;
				} else if (waves.Count == 0 && timeToStartPlanet <= 0) {
					timeToStartPlanet = (float)(totalTime + delayBetweenLastWaveAndPlanet);
				}else if(planetStarted && planetWaves.Count > 0){
					timeSincePlanetWave += LevelManager.ScaledTime ();
					if (planetWaves [planetWaveIndex].timeTrigger <= timeSincePlanetWave) {
						Wave planetWave = planetWaves [planetWaveIndex];

						for(int i=0; i<planetWave.spawns.Count; i++){
							SpawnDebris(planetWave.spawns[i]);
						}

						if (planetWave.waveType == WaveEnum.WaveType.PREFAB_RAIN) {
							rainingPrefabs = true;
							rainDuration = planetWave.duration;
							rainFrequency = planetWave.frequency;
							rainElapsed = 0f;
							rainPrefab = planetWave.prefab;
						}else if(planetWave.waveType == WaveEnum.WaveType.SPLINE) {
							GameObject debris = ObjectPoolingManager.Instance.GetSpline ();
							SplineDebrisCreator spline = debris.GetComponent<SplineDebrisCreator>();
							if(spline != null){
								spline.SetFromSpawnData(planetWave.splineData);
								float halfWidth = width * 0.5f;
								Vector3 point = new Vector3 (transform.position.x / (halfWidth), 0, 0);
								spline.SpawnSpline (point, halfWidth);
							}
						}

						planetWaveIndex++;
						if (planetWaveIndex >= planetWaves.Count) {
							planetWaveIndex = 0;
							timeSincePlanetWave = 0;
						}
					}
				}
			}

			if(rainingPrefabs){
				timeSinceLastRainSpawn += LevelManager.ScaledTime();
				if (timeSinceLastRainSpawn >= rainFrequency) {
					GameObject debris = GameObject.Instantiate(rainPrefab);
					ColorHolder colorHolder = debris.GetComponent<ColorHolder> ();
					if (colorHolder != null) {
						colorHolder.SetColor(Random.Range((int)PhotonEnum.PhotonColor.ULTRAVIOLET, (int)PhotonEnum.PhotonColor.INFRARED));
					}
					Vector3 pos = debris.transform.position;
					pos.x = Random.Range (xMin, xMax);
					pos.y = 0f;
					pos.z = 0f;
					debris.transform.position = pos;
					timeSinceLastRainSpawn = 0;
				}
				rainElapsed += LevelManager.ScaledTime();
				if (rainElapsed >= rainDuration) {
					rainingPrefabs = false;
				}
			}
		}
	}

	private void SpawnDebris(Spawn spawn){
		GameObject debris = null;
		SplineDebrisCreator spline = null;
		Vector3 pos = spawn.position;
		float halfWidth = width * 0.5f;
		pos.x = ((transform.position.x / (halfWidth)) +  spawn.position.x) * halfWidth;

		switch(spawn.type){
			case SpawnType.ROCK:
				debris = ObjectPoolingManager.Instance.GetRock();
				break;

			case SpawnType.CRYSTAL:
				debris = ObjectPoolingManager.Instance.GetCrystal();
				break;

			case SpawnType.DUST:
				debris = ObjectPoolingManager.Instance.GetDust();
				break;

			case SpawnType.RADIATION:
				debris = ObjectPoolingManager.Instance.GetRad();
				break;

            case SpawnType.PREFAB:
                debris = Instantiate(spawn.prefab);
                spline = debris.GetComponent<SplineDebrisCreator>();
                if(spline != null){
                	spline.Color = spawn.color;
					spline.SpawnSpline (pos, halfWidth);
                }
                break;

			case SpawnType.SPLINE:
                debris = ObjectPoolingManager.Instance.GetSpline();
                spline = debris.GetComponent<SplineDebrisCreator>();
                if (spline != null) {
					Debug.Log("spline as spawn with velocity = " + spawn.splineData.velocity);
                    spline.SetFromSpawnData(spawn.splineData);
                    spline.SpawnSpline(pos,halfWidth);
                }
				break;
		}

		if(debris == null){
			return;
        }

		Vector3 scale = debris.transform.localScale * spawn.scale;
        debris.transform.localScale = scale;
		debris.transform.position = pos;

		ObjectMover mover = debris.GetComponent<ObjectMover> ();
		if(mover == null){
			mover = debris.GetComponentInChildren<ObjectMover> ();
		}

		if (spawn.velocity == new Vector3 ()) {
			spawn.velocity = defaultVelocity;
		}

		if (mover != null) {
			Vector3 velocityToSet = spawn.velocity;
			if(mover.velocity.Equals(new Vector3())){
				velocityToSet = new Vector3 (0, 0, -4);
			}

			mover.SetVelocity (velocityToSet);
		}

		Destructable destructable = debris.GetComponent<Destructable> ();
		if (destructable != null) {
			destructable.SetMaxHealth (spawn.maxHealth);
		} else {
			ClusterHolder cluster = debris.GetComponent<ClusterHolder> ();
			if (cluster != null) {
				cluster.SetMaxHealth (spawn.maxHealth);
			}
		}

		ColorHolder colorHolder = debris.GetComponent<ColorHolder> ();
		if (colorHolder == null) {
			colorHolder = debris.GetComponentInChildren<ColorHolder> ();
		}
		if (colorHolder != null) {
			colorHolder.SetPhotonColor (spawn.color);
		}
	}
	
	private void PopulateWaves(){
		List<Wave> populatedWaves = new List<Wave>();

		foreach(Wave wave in waves){
			switch(wave.waveType){
				case WaveEnum.WaveType.MANUAL:
					populatedWaves.Add(wave);
					break;

				case WaveEnum.WaveType.SPLINE:
					populatedWaves.Add (wave);
					break;
					
				case WaveEnum.WaveType.FIVE_TIMES_FIVE_ROCK:
					List<Wave> thisWave = Wave.GenerateNByNRockWaves(5, -1, 1);
					float time = wave.timeTrigger;
					foreach(Wave w in thisWave){
						w.timeTrigger = time;
						time++;
						populatedWaves.Add(w);
					}
					break;

                case WaveEnum.WaveType.FIVE_CRYSTAL:
                    Wave crystalWave = Wave.GenerateWaveWithCrystals(-1, 1, wave.color);
					crystalWave.timeTrigger = wave.timeTrigger;
					populatedWaves.Add(crystalWave);
                    break;

				case WaveEnum.WaveType.PREFAB_RAIN:
					populatedWaves.Add(wave);
					break;

				case WaveEnum.WaveType.ROCK_HELIX:
					Wave rocks = Wave.GenerateRockHelix(-1, 1, wave.frequency, (int)wave.duration);
					rocks.timeTrigger = wave.timeTrigger;
					populatedWaves.Add(rocks);
					break;

				case WaveEnum.WaveType.FIVE_OF_PREFAB:
				if(wave.spawns.Count > 0){
					GameObject prefab = wave.prefab;
					if(prefab != null){
						Wave fiveOf = Wave.GenerateFiveOfPrefab(-1, 1, wave.frequency, prefab);
						fiveOf.timeTrigger = wave.timeTrigger;
						populatedWaves.Add(fiveOf);
					}
				}
					break;
			}
		}
		
		waves = populatedWaves;
	}
}