﻿using UnityEngine;
using System.Collections;

public class DeactivateAfterTime : MonoBehaviour {
	public float time = 1f;

	private float elapsed = 0f;

	public void Reset(){
		elapsed = 0f;
	}

	void Update () {
		if (DataCenter.Instance.GameState == DataCenter.STATE_STARTED) {
			elapsed += Time.deltaTime;

			if (elapsed >= time) {
				transform.gameObject.SetActive (false);
			}
		}
	}
}
