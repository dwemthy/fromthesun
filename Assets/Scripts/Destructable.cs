﻿using UnityEngine;
using System.Collections;

public class Destructable : MonoBehaviour {
	public AudioSource destroyedSource;
	public int damageDealt = 1;
	public int maxHealth = 1;

	public void SetMaxHealth(int max){
		maxHealth = max;
		health = max;
	}

	protected int health;
	public int Health {
		get { return health; }
	}

	void Start(){
		health = maxHealth;
	}

	public void Revive(){
		health = maxHealth;
		transform.gameObject.SetActive (true);

		if(GetComponent<Collider>()) {
			GetComponent<Collider>().enabled = true;
		}

		GetComponent<Renderer>().enabled = true;
	}

	void OnTriggerEnter(Collider other){
		if(DataCenter.Instance.GameState == DataCenter.STATE_STARTED && health > 0){
			if (other.gameObject.tag.Equals ("Player") ||
			   other.gameObject.tag.Equals ("Photon")) {
				bool killed = false;
				ColorHolder colorHolder = GetComponent<ColorHolder> ();
				if (colorHolder != null) {
					killed = colorHolder.isKilledByPlayer (); 
				} else {
					killed = true;
				}

				if (killed) {
					PlayerPhotonController player = other.gameObject.GetComponent<PlayerPhotonController> ();
					if (player != null) {
						Damage (health);
						player.Damage (damageDealt);
					} else {
						other.gameObject.SetActive (false);
					}
				}
			}
		}
	}

	public virtual int Damage(int damage){
		int dealt = damage;
		if (health <= 0) {
			dealt = 0;
		} else if (damage >= health) {
            Die();
			dealt = health;
		}

		health -= dealt;

		return dealt;
	}

    protected virtual void Die() {
        if (destroyedSource != null)
        {
            destroyedSource.Play();
        }

        LevelManager.Instance.AddScore(maxHealth * 50, transform.position);
        SpawnTetras();

        gameObject.SetActive(false);
    }

	protected void SpawnTetras(){
		MeshRenderer myRenderer = GetComponent<MeshRenderer> ();
		float scale = transform.localScale.magnitude;
		int count = Mathf.CeilToInt(4f * scale);
		if (myRenderer != null) {
			for (int i = 0; i < count; i++) {
				GameObject tetra = ObjectPoolingManager.Instance.GetRockTetra ();
				if (tetra != null) {
					tetra.transform.position = transform.position;
					tetra.transform.localScale = transform.localScale * 0.5f;

					ObjectMover mover = tetra.GetComponent<ObjectMover> ();
					if (mover != null) {

						Vector3 tetraVel = new Vector3(Random.Range(-2f, 2f), Random.Range(0f, 0.5f), Random.Range(0f, 4f));
						mover.velocity = tetraVel;
					}

					MeshRenderer renderer = tetra.GetComponent<MeshRenderer> ();
					if (renderer != null) {
						renderer.material = myRenderer.material;
					}

					DeactivateAfterTime dAT = tetra.GetComponent<DeactivateAfterTime> ();
					if (dAT != null) {
						dAT.time = Random.Range (0.1f, 1f);
						dAT.Reset ();
					}
				}
			}
		}
	}
}