﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/PlanetShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_PiecePos ("PiecePos", Vector) = (0,0,0,0)
		_Size ("Size", float) = 1
	}
	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			
			sampler2D _MainTex;

			fixed4 _PiecePos;
			float _Size;
			fixed4 _MainTex_ST;


			struct appdata
			{
				float4 pos : POSITION;
			};

			struct v2f
			{
				half2 uv : TEXCOORD0;
				float4 pos : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;

				o.pos = UnityObjectToClipPos(v.pos);

				//Create uv at center
				half2 uv = half2(0.5, 0.5);
				//Move concept of center to match this piece's position within the planet
				uv = uv + (_PiecePos.xz / _Size);
				//Now move the uv based on the vertex, adjusting for size
				uv = uv  + (v.pos.xz / _Size);

				o.uv = uv;

				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				return col;
			}
			ENDCG
		}
	}
}
