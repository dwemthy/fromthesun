﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/ProgressGaugeShader" {
	Properties {
		_MainTex ("Texture)", 2D) = "white" {}
		_MinX("MinX", float) = 0
		_MaxX("MaxX", float) = 1
		_MaxY("MaxY", float) = 1
	}
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
         ZWrite Off
         Blend SrcAlpha OneMinusSrcAlpha
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float _MinX;
			float _MaxX;
			float _MaxY;
			
			#include "UnityCG.cginc"
			
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};
			
			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 wpos : TEXCOORD1;
			};
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.wpos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}
			
			sampler2D _MainTex;
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				float3 pos = i.wpos;

				if(pos.z <= _MaxY){
					float width = _MaxX - _MinX;
					float borderSize = width * 0.02;
					float midX = _MaxX - (width * 0.5);
					float yLimitOffset;
					if(pos.x < midX){
						yLimitOffset = pos.x - midX;
					}else{
						yLimitOffset = midX - pos.x;
					}

					if(pos.z - borderSize > _MaxY + yLimitOffset){
						//Make the pointy part
						col = fixed4(0,0,0,0);
					}else if(pos.z > _MaxY + yLimitOffset ||
						pos.x < _MinX + borderSize ||
						pos.x > _MaxX - borderSize){
						//Draw black borders
						col = fixed4(0,0,0,1);
					}
				}else{
					col = fixed4(0,0,0,0);
				}

				return col;
			}
			ENDCG
		}
	}
}