﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/StaticShader" {
	Properties {
		_MainTex("Texture)", 2D) = "white" {}
		_ColorA("Color A", Color) = (1,1,1,1)
		_ColorB("Color B", Color) = (0,0,0,0)

		_ResX("X Resolution", Float) = 0.1
		_ResY("Y Resolution", Float) = 0.1

		_Scale("Scale", Float) = 1600
		_Percent("Percent", Float) = 0
	}

	SubShader {
		Cull Off ZWrite Off ZTest Always
		
		Pass {
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			//This produces random values between 0 and 1
			float rand(float2 co) {
				return frac((sin(dot(co.xy , float2(12.345 * _Time.w, 67.890 * _Time.w))) * 12345.67890 + _Time.w));
			}

			fixed4 _ColorA;
			fixed4 _ColorB;

			float _ResX;
			float _ResY;

			float _Scale;
			float _Percent;

			sampler2D _MainTex;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);

				fixed4 sc = fixed4(i.uv, 0.0, 1.0);
				sc.x -= 0.75;
				sc.y -= 0.75;

				sc *= 1600;

				//round the screen coordinates to give it a blocky appearance
				sc.x = round(sc.x*_ResX) / _ResX;
				sc.y = round(sc.y*_ResY) / _ResY;

				float noise = rand(sc.xy);
				float4 stat = lerp(_ColorA, _ColorB, noise.x);

				col = lerp(col, stat, _Percent);

				return col;
			}

			ENDCG
		}
	}
}
